
(function(window, Ivp){
  'use strict';

  var scope, node, settings, frames, active, template;

  var onended = function(e) {
    var next;
    var last = _.last(frames);
    if (last.id === e.fr) {
      next = frames[0];
    } else {
      var index = _.findIndex(frames, {id: e.fr});
      next = frames[index+1];
    }

    setActive(document.querySelector('.ivp-playlist-item[data-id="'+ next.id +'"]'));

    scope.player.set(next.id);

    scope.play();
  };

  var setActive = function(el) {
    // get the previously active frame(s)
    var activeNode = document.querySelectorAll('.ivp-playlist-item.active');
    _.forEach(activeNode, function(node) {
			node.classList.remove('active');
		});
    el.classList.add('active');
  };

  var clickHandler = function(e) {
    e.stopPropagation();
    var el = Ivp.Utility.findParent('ivp-playlist-item', e);
    // you clicked the active video - so im going to stop working
    if (el.dataset.id === scope.player.activeFrame().id) {
			return;
		}
    // Set the active element
    setActive(el);
    // Set the new video frame
    scope.player.set(el.dataset.id);
  };

  var findFrame = function(id) {
    var index = _.findIndex(frames, {id: id});
    return (index > -1) ? frames[index] : false;
  };

  // create a thumbnail with a frames poster image
  var thumbnail = function(id) {
    var config = findFrame(id);
    if (config) {
      return config.poster;
    }
  };

  // I start everything
  var init = function(element) {
    // Ivp Manager instance is the scope
    scope = this;
    // store the playlist element
    node = element;
    // get the instance config data
    settings = scope.settings();
    // Get the active video frame data
    active = scope.player.activeFrame();
    // Get all the video frames
    frames = scope.player.frames();
    // This plugin requires a key titled playlist, to be
    // attached to the Ivp settings. This key should contain
    // a object literal.
    if (!_.isPlainObject(settings.data.playlist)) {
      return;
    }
    var parent = Ivp.Element.add('div', 'ivp-playlist-wrapper');
    // Use Ivp template interface for parsing templates
    var template = new Ivp.Templater(settings.data.playlist);
    // Create each playlist item
    _.forEach(settings.data.playlist.items, function(item) {
      var el = Ivp.Element.add('div', 'ivp-playlist-item');
      // If the playlist config doesn't have an image
      // use the video id's poster image instead
      if (!item.data.hasOwnProperty('image')) {
       item.data.image = thumbnail(item.id);
      }
      el.innerHTML = template.renderTemplate(item);
      el.setAttribute('data-id', item.id);
      // Check if this video is currently active
      if (item.id === active.id) {
        el.classList.add('active');
      }
      // Attach the click handler
      el.addEventListener('click', clickHandler, false);
      parent.appendChild(el);
    });

    node.appendChild(parent);
    // Listen for a video end event
    scope.observer.on('ended', onended);
  };

  // register the plugin
  Ivp.plugin('playlist', init);

})(window, window.Invodo.Ixd.Ivp);
