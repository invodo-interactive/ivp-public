'use strict';
/**
 * dataset polyfil for Ie 9 - 10
 * @author remy
 * @link https://raw.githubusercontent.com/remy/polyfills/master/dataset.js
 */

 /**
  * Add dataset support to elements
  * No globals, no overriding prototype with non-standard methods,
  *   handles CamelCase properly, attempts to use standard
  *   Object.defineProperty() (and Function bind()) methods,
  *   falls back to native implementation when existing
  * Inspired by http://code.eligrey.com/html5/dataset/
  *   (via https://github.com/adalgiso/html5-dataset/blob/master/html5-dataset.js )
  * Depends on Function.bind and Object.defineProperty/Object.getOwnPropertyDescriptor (shims below)
  * Licensed under the X11/MIT License
 */

 // Inspired by https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Function/bind#Compatibility
 if (!Function.prototype.bind) {
     Function.prototype.bind = function (oThis) {
         'use strict';
         if (typeof this !== "function") {
             // closest thing possible to the ECMAScript 5 internal IsCallable function
             throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
         }

         var aArgs = Array.prototype.slice.call(arguments, 1),
             fToBind = this,
             FNOP = function () {},
             fBound = function () {
                 return fToBind.apply(
                     this instanceof FNOP && oThis ? this : oThis,
                    aArgs.concat(Array.prototype.slice.call(arguments))
                );
             };

         FNOP.prototype = this.prototype;
         fBound.prototype = new FNOP();

         return fBound;
     };
 }

 /*
  * Xccessors Standard: Cross-browser ECMAScript 5 accessors
  * http://purl.eligrey.com/github/Xccessors
  *
  * 2010-06-21
  *
  * By Eli Grey, http://eligrey.com
  *
  * A shim that partially implements Object.defineProperty,
  * Object.getOwnPropertyDescriptor, and Object.defineProperties in browsers that have
  * legacy __(define|lookup)[GS]etter__ support.
  *
  * Licensed under the X11/MIT License
  *   See LICENSE.md
 */

 // Removed a few JSLint options as Notepad++ JSLint validator complaining and
 //   made comply with JSLint; also moved 'use strict' inside function
 /*jslint white: true, undef: true, plusplus: true,
   bitwise: true, regexp: true, newcap: true, maxlen: 90 */

 /*! @source http://purl.eligrey.com/github/Xccessors/blob/master/xccessors-standard.js*/

 (function () {
     'use strict';
     var ObjectProto = Object.prototype,
     defineGetter = ObjectProto.__defineGetter__,
     defineSetter = ObjectProto.__defineSetter__,
     lookupGetter = ObjectProto.__lookupGetter__,
     lookupSetter = ObjectProto.__lookupSetter__,
     hasOwnProp = ObjectProto.hasOwnProperty;

     if (defineGetter && defineSetter && lookupGetter && lookupSetter) {

         if (!Object.defineProperty) {
             Object.defineProperty = function (obj, prop, descriptor) {
                 if (arguments.length < 3) { // all arguments required
                     throw new TypeError("Arguments not optional");
                 }

                 prop += ""; // convert prop to string

                 if (hasOwnProp.call(descriptor, "value")) {
                     if (!lookupGetter.call(obj, prop) && !lookupSetter.call(obj, prop)) {
                         // data property defined and no pre-existing accessors
                         obj[prop] = descriptor.value;
                     }

                     if ((hasOwnProp.call(descriptor, "get") ||
                          hasOwnProp.call(descriptor, "set")))
                     {
                         // descriptor has a value prop but accessor already exists
                         throw new TypeError("Cannot specify an accessor and a value");
                     }
                 }

                 // can't switch off these features in ECMAScript 3
                 // so throw a TypeError if any are false
                 if (!(descriptor.writable && descriptor.enumerable &&
                     descriptor.configurable))
                 {
                     throw new TypeError(
                         "This implementation of Object.defineProperty does not support" +
                         " false for configurable, enumerable, or writable."
                     );
                 }

                 if (descriptor.get) {
                     defineGetter.call(obj, prop, descriptor.get);
                 }
                 if (descriptor.set) {
                     defineSetter.call(obj, prop, descriptor.set);
                 }

                 return obj;
             };
         }

         if (!Object.getOwnPropertyDescriptor) {
             Object.getOwnPropertyDescriptor = function (obj, prop) {
                 if (arguments.length < 2) { // all arguments required
                     throw new TypeError("Arguments not optional.");
                 }

                 prop += ""; // convert prop to string

                 var descriptor = {
                     configurable: true,
                     enumerable  : true,
                     writable    : true
                 },
                 getter = lookupGetter.call(obj, prop),
                 setter = lookupSetter.call(obj, prop);

                 if (!hasOwnProp.call(obj, prop)) {
                     // property doesn't exist or is inherited
                     return descriptor;
                 }
                 if (!getter && !setter) { // not an accessor so return prop
                     descriptor.value = obj[prop];
                     return descriptor;
                 }

                 // there is an accessor, remove descriptor.writable;
                 // populate descriptor.get and descriptor.set (IE's behavior)
                 delete descriptor.writable;
                 descriptor.get = descriptor.set = undefined;

                 if (getter) {
                     descriptor.get = getter;
                 }
                 if (setter) {
                     descriptor.set = setter;
                 }

                 return descriptor;
             };
         }

         if (!Object.defineProperties) {
             Object.defineProperties = function (obj, props) {
                 var prop;
                 for (prop in props) {
                     if (hasOwnProp.call(props, prop)) {
                         Object.defineProperty(obj, prop, props[prop]);
                     }
                 }
             };
         }
     }
 }());

 // Begin dataset code

 if (!document.documentElement.dataset &&
          // FF is empty while IE gives empty object
         (!Object.getOwnPropertyDescriptor(Element.prototype, 'dataset')  ||
         !Object.getOwnPropertyDescriptor(Element.prototype, 'dataset').get)
     ) {
     var propDescriptor = {
         enumerable: true,
         get: function () {
             'use strict';
             var i,
                 that = this,
                 HTML5_DOMStringMap,
                 attrVal, attrName, propName,
                 attribute,
                 attributes = this.attributes,
                 attsLength = attributes.length,
                 toUpperCase = function (n0) {
                     return n0.charAt(1).toUpperCase();
                 },
                 getter = function () {
                     return this;
                 },
                 setter = function (attrName, value) {
                     return (typeof value !== 'undefined') ?
                         this.setAttribute(attrName, value) :
                         this.removeAttribute(attrName);
                 };
             try { // Simulate DOMStringMap w/accessor support
                 // Test setting accessor on normal object
                 ({}).__defineGetter__('test', function () {});
                 HTML5_DOMStringMap = {};
             }
             catch (e1) { // Use a DOM object for IE8
                 HTML5_DOMStringMap = document.createElement('div');
             }
             for (i = 0; i < attsLength; i++) {
                 attribute = attributes[i];
                 // Fix: This test really should allow any XML Name without
                 //         colons (and non-uppercase for XHTML)
                 if (attribute && attribute.name &&
                     (/^data-\w[\w\-]*$/).test(attribute.name)) {
                     attrVal = attribute.value;
                     attrName = attribute.name;
                     // Change to CamelCase
                     propName = attrName.substr(5).replace(/-./g, toUpperCase);
                     try {
                         Object.defineProperty(HTML5_DOMStringMap, propName, {
                             enumerable: this.enumerable,
                             get: getter.bind(attrVal || ''),
                             set: setter.bind(that, attrName)
                         });
                     }
                     catch (e2) { // if accessors are not working
                         HTML5_DOMStringMap[propName] = attrVal;
                     }
                 }
             }
             return HTML5_DOMStringMap;
         }
     };
     try {
         // FF enumerates over element's dataset, but not
         //   Element.prototype.dataset; IE9 iterates over both
         Object.defineProperty(Element.prototype, 'dataset', propDescriptor);
     } catch (e) {
         propDescriptor.enumerable = false; // IE8 does not allow setting to true
         Object.defineProperty(Element.prototype, 'dataset', propDescriptor);
     }
 }

/**
 * classList polyfil for IE 9
 * @author remy
 * @link https://github.com/remy/polyfills/blob/master/classList.js
 */
 /*
  * classList.js: Cross-browser full element.classList implementation.
  * 1.1.20150312
  *
  * By Eli Grey, http://eligrey.com
  * License: Dedicated to the public domain.
  *   See https://github.com/eligrey/classList.js/blob/master/LICENSE.md
  */

 /*global self, document, DOMException */

 /*! @source http://purl.eligrey.com/github/classList.js/blob/master/classList.js */

 if ("document" in self) {

 // Full polyfill for browsers with no classList support
 if (!("classList" in document.createElement("_"))) {

 (function (view) {

 "use strict";

 if (!('Element' in view)) return;

 var
 	  classListProp = "classList"
 	, protoProp = "prototype"
 	, elemCtrProto = view.Element[protoProp]
 	, objCtr = Object
 	, strTrim = String[protoProp].trim || function () {
 		return this.replace(/^\s+|\s+$/g, "");
 	}
 	, arrIndexOf = Array[protoProp].indexOf || function (item) {
 		var
 			  i = 0
 			, len = this.length
 		;
 		for (; i < len; i++) {
 			if (i in this && this[i] === item) {
 				return i;
 			}
 		}
 		return -1;
 	}
 	// Vendors: please allow content code to instantiate DOMExceptions
 	, DOMEx = function (type, message) {
 		this.name = type;
 		this.code = DOMException[type];
 		this.message = message;
 	}
 	, checkTokenAndGetIndex = function (classList, token) {
 		if (token === "") {
 			throw new DOMEx(
 				  "SYNTAX_ERR"
 				, "An invalid or illegal string was specified"
 			);
 		}
 		if (/\s/.test(token)) {
 			throw new DOMEx(
 				  "INVALID_CHARACTER_ERR"
 				, "String contains an invalid character"
 			);
 		}
 		return arrIndexOf.call(classList, token);
 	}
 	, ClassList = function (elem) {
 		var
 			  trimmedClasses = strTrim.call(elem.getAttribute("class") || "")
 			, classes = trimmedClasses ? trimmedClasses.split(/\s+/) : []
 			, i = 0
 			, len = classes.length
 		;
 		for (; i < len; i++) {
 			this.push(classes[i]);
 		}
 		this._updateClassName = function () {
 			elem.setAttribute("class", this.toString());
 		};
 	}
 	, classListProto = ClassList[protoProp] = []
 	, classListGetter = function () {
 		return new ClassList(this);
 	}
 ;
 // Most DOMException implementations don't allow calling DOMException's toString()
 // on non-DOMExceptions. Error's toString() is sufficient here.
 DOMEx[protoProp] = Error[protoProp];
 classListProto.item = function (i) {
 	return this[i] || null;
 };
 classListProto.contains = function (token) {
 	token += "";
 	return checkTokenAndGetIndex(this, token) !== -1;
 };
 classListProto.add = function () {
 	var
 		  tokens = arguments
 		, i = 0
 		, l = tokens.length
 		, token
 		, updated = false
 	;
 	do {
 		token = tokens[i] + "";
 		if (checkTokenAndGetIndex(this, token) === -1) {
 			this.push(token);
 			updated = true;
 		}
 	}
 	while (++i < l);

 	if (updated) {
 		this._updateClassName();
 	}
 };
 classListProto.remove = function () {
 	var
 		  tokens = arguments
 		, i = 0
 		, l = tokens.length
 		, token
 		, updated = false
 		, index
 	;
 	do {
 		token = tokens[i] + "";
 		index = checkTokenAndGetIndex(this, token);
 		while (index !== -1) {
 			this.splice(index, 1);
 			updated = true;
 			index = checkTokenAndGetIndex(this, token);
 		}
 	}
 	while (++i < l);

 	if (updated) {
 		this._updateClassName();
 	}
 };
 classListProto.toggle = function (token, force) {
 	token += "";

 	var
 		  result = this.contains(token)
 		, method = result ?
 			force !== true && "remove"
 		:
 			force !== false && "add"
 	;

 	if (method) {
 		this[method](token);
 	}

 	if (force === true || force === false) {
 		return force;
 	} else {
 		return !result;
 	}
 };
 classListProto.toString = function () {
 	return this.join(" ");
 };

 if (objCtr.defineProperty) {
 	var classListPropDesc = {
 		  get: classListGetter
 		, enumerable: true
 		, configurable: true
 	};
 	try {
 		objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
 	} catch (ex) { // IE 8 doesn't support enumerable:true
 		if (ex.number === -0x7FF5EC54) {
 			classListPropDesc.enumerable = false;
 			objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
 		}
 	}
 } else if (objCtr[protoProp].__defineGetter__) {
 	elemCtrProto.__defineGetter__(classListProp, classListGetter);
 }

 }(self));

 } else {
 // There is full or partial native classList support, so just check if we need
 // to normalize the add/remove and toggle APIs.

 (function () {
 	"use strict";

 	var testElement = document.createElement("_");

 	testElement.classList.add("c1", "c2");

 	// Polyfill for IE 10/11 and Firefox <26, where classList.add and
 	// classList.remove exist but support only one argument at a time.
 	if (!testElement.classList.contains("c2")) {
 		var createMethod = function(method) {
 			var original = DOMTokenList.prototype[method];

 			DOMTokenList.prototype[method] = function(token) {
 				var i, len = arguments.length;

 				for (i = 0; i < len; i++) {
 					token = arguments[i];
 					original.call(this, token);
 				}
 			};
 		};
 		createMethod('add');
 		createMethod('remove');
 	}

 	testElement.classList.toggle("c3", false);

 	// Polyfill for IE 10 and Firefox <24, where classList.toggle does not
 	// support the second argument.
 	if (testElement.classList.contains("c3")) {
 		var _toggle = DOMTokenList.prototype.toggle;

 		DOMTokenList.prototype.toggle = function(token, force) {
 			if (1 in arguments && !this.contains(token) === !force) {
 				return force;
 			} else {
 				return _toggle.call(this, token);
 			}
 		};

 	}

 	testElement = null;
 }());

 }

 }


/**
 * CustomEvent polyfil for IE 9
 * @author MDN
 * @link https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent
 */
(function () {
  function CustomEvent ( event, params ) {
    params = params || { bubbles: false, cancelable: false, detail: undefined };
    var evt = document.createEvent( 'CustomEvent' );
    evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
    return evt;
   }

  CustomEvent.prototype = window.Event.prototype;

  window.CustomEvent = CustomEvent;
})();

/**
 * requestAnimationFrame polyfill
 * @author Erik Möller. fixes from Paul Irish and Tino Zijdel
 * @link https://gist.github.com/paulirish/1579671
 */
(function() {
var lastTime = 0;
var vendors = ['ms', 'moz', 'webkit', 'o'];
for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                               || window[vendors[x]+'CancelRequestAnimationFrame'];
}

if (!window.requestAnimationFrame)
  window.requestAnimationFrame = function(callback, element) {
      var currTime = new Date().getTime();
      var timeToCall = Math.max(0, 16 - (currTime - lastTime));
      var id = window.setTimeout(function() { callback(currTime + timeToCall); },
        timeToCall);
      lastTime = currTime + timeToCall;
      return id;
  };

if (!window.cancelAnimationFrame)
  window.cancelAnimationFrame = function(id) {
      clearTimeout(id);
  };
}());

/**
 * xhr-xdr-adapter
 * @author intuit
 * @link https://github.com/intuit/xhr-xdr-adapter/blob/master/src/xhr-xdr-adapter.js
 */
(function () {
    "use strict";
    // Ignore everything below if not on IE 8 or IE 9.
    if (!window.XDomainRequest) {  // typeof XDomainRequest is 'object' in IE 8, 'function' in IE 9
        return;
    }
    if ('withCredentials' in new window.XMLHttpRequest()) {
        return;
    }
    if (window.XMLHttpRequest.supportsXDR === true) {
        // already set up
        return;
    }

    var OriginalXMLHttpRequest = window.XMLHttpRequest;
    var urlRegEx = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/;
    var httpRegEx = /^https?:\/\//i;
    var getOrPostRegEx = /^get|post$/i;
    var sameSchemeRegEx = new RegExp('^' + location.protocol, 'i');

    // Determine whether XDomainRequest should be used for a given request
    var useXDR = function (method, url, async) {
        var remoteUrl = url;
        var baseHref;
        var myLocationParts;
        var remoteLocationParts;
        var crossDomain;

        try {
            // check to see if this is a "same-scheme" URL ("//example.com").
            // prepend location.protocol if so.
            if (remoteUrl[0] === '/' && remoteUrl[1] === '/') {
                remoteUrl = location.protocol + remoteUrl;
            }

            // account for the possibility of a <base href="..."> setting, which could make a URL that looks relative actually be cross-domain
            if ((remoteUrl && remoteUrl.indexOf("://") < 0) && document.getElementsByTagName('base').length > 0) {
                baseHref = document.getElementsByTagName('base')[0].href;
                if (baseHref) {
                    remoteUrl = baseHref + remoteUrl;
                }
            }

            myLocationParts = urlRegEx.exec(location.href);
            remoteLocationParts = urlRegEx.exec(remoteUrl);
            crossDomain = (myLocationParts[2].toLowerCase() !== remoteLocationParts[2].toLowerCase());

            // XDomainRequest can only be used for async get/post requests across same scheme, which must be http: or https:
            return crossDomain && async && getOrPostRegEx.test(method) && httpRegEx.test(remoteUrl) && sameSchemeRegEx.test(remoteUrl);
        }
        catch (ex) {
            return false;
        }
    };

    window.XMLHttpRequest = function () {
        var self = this;
        this._setReadyState = function (readyState) {
            if (self.readyState !== readyState) {
                self.readyState = readyState;
                if (typeof self.onreadystatechange === "function") {
                    self.onreadystatechange();
                }
            }
        };
        this.readyState = 0;
        this.responseText = "";
        this.status = 0;
        this.statusText = "";
        this.withCredentials = false;
    };

    window.XMLHttpRequest.prototype.open = function (method, url, async) {
        var self = this;
        var request;

        if (useXDR(method, url, async)) {
            // Use XDR
            request = new XDomainRequest();
            request._xdr = true;
            request.onerror = function () {
                self.status = 400;
                self.statusText = "Error";
                self._setReadyState(4);
                if (self.onerror) {
                    self.onerror();
                }
            };
            request.ontimeout = function () {
                self.status = 408;
                self.statusText = "Timeout";
                self._setReadyState(2);
                if (self.ontimeout) {
                    self.ontimeout();
                }
            };
            request.onload = function () {
                self.responseText = request.responseText;
                self.status = 200;
                self.statusText = "OK";
                self._setReadyState(4);
                if (self.onload) {
                    self.onload();
                }
            };
            request.onprogress = function () {
                if (self.onprogress) {
                    self.onprogress.apply(self, arguments);
                }
            };
        }

        else {
            // Use standard XHR
            request = new OriginalXMLHttpRequest();
            request.withCredentials = this.withCredentials;
            request.onreadystatechange = function () {
                if (request.readyState === 4) {
                    try {
                        self.status = request.status;
                        self.statusText = request.statusText;
                        self.responseText = request.responseText;
                        self.responseXML = request.responseXML;
                    } catch (e) {

                    }
                }
                self._setReadyState(request.readyState);
            };
            request.onabort = function () {
                if (self.onabort) {
                    self.onabort.apply(self, arguments);
                }
            };
            request.onerror = function () {
                if (self.onerror) {
                    self.onerror.apply(self, arguments);
                }
            };
            request.onload = function () {
                if (self.onload) {
                    self.onload.apply(self, arguments);
                }
            };
            request.onloadend = function () {
                if (self.onloadend) {
                    self.onloadend.apply(self, arguments);
                }
            };
            request.onloadstart = function () {
                if (self.onloadstart) {
                    self.onloadstart.apply(self, arguments);
                }
            };
            request.onprogress = function () {
                if (self.onprogress) {
                    self.onprogress.apply(self, arguments);
                }
            };
        }

        this._request = request;
        request.open.apply(request, arguments);
        this._setReadyState(1);
    };

    window.XMLHttpRequest.prototype.abort = function () {
        this._request.abort();
        this._setReadyState(0);
        this.onreadystatechange = null;
    };

    window.XMLHttpRequest.prototype.send = function (body) {
        var self = this;
        this._request.withCredentials = this.withCredentials;

        if (this._request._xdr) {
            setTimeout(function () {
                self._request.send(body);
            }, 0);
        }
        else {
            this._request.send(body);
        }

        if (this._request.readyState === 4) {
            // when async==false the browser is blocked until the transfer is complete and readyState becomes 4
            // onreadystatechange should not get called in this case
            this.status = this._request.status;
            this.statusText = this._request.statusText;
            this.responseText = this._request.responseText;
            this.readyState = this._request.readyState;
        }
        else {
            this._setReadyState(2);
        }
    };

    window.XMLHttpRequest.prototype.setRequestHeader = function () {
        if (this._request.setRequestHeader) {
            this._request.setRequestHeader.apply(this._request, arguments);
        }
    };

    window.XMLHttpRequest.prototype.getAllResponseHeaders = function () {
        if (this._request.getAllResponseHeaders) {
            return this._request.getAllResponseHeaders();
        }
        else {
            return ("Content-Length: " + this.responseText.length +
                "\r\nContent-Type:" + this._request.contentType);
        }
    };

    window.XMLHttpRequest.prototype.getResponseHeader = function (header) {
        if (this._request.getResponseHeader) {
            return this._request.getResponseHeader.apply(this._request, arguments);
        }
        if (typeof  header !== "string") {
            return;
        }
        header = header.toLowerCase();
        if (header === "content-type") {
            return this._request.contentType;
        }
        else if (header === "content-length") {
            return this.responseText.length;
        }
    };

    window.XMLHttpRequest.supportsXDR = true;
})();

/**
 * lie.polyfill.js
 * @author calvinmetcalf
 * @link https://github.com/calvinmetcalf/lie/blob/master/dist/lie.polyfill.js
 */
(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
'use strict';

module.exports = INTERNAL;

function INTERNAL() {}
},{}],2:[function(_dereq_,module,exports){
'use strict';
var Promise = _dereq_('./promise');
var reject = _dereq_('./reject');
var resolve = _dereq_('./resolve');
var INTERNAL = _dereq_('./INTERNAL');
var handlers = _dereq_('./handlers');
module.exports = all;
function all(iterable) {
  if (Object.prototype.toString.call(iterable) !== '[object Array]') {
    return reject(new TypeError('must be an array'));
  }

  var len = iterable.length;
  var called = false;
  if (!len) {
    return resolve([]);
  }

  var values = new Array(len);
  var resolved = 0;
  var i = -1;
  var promise = new Promise(INTERNAL);

  while (++i < len) {
    allResolver(iterable[i], i);
  }
  return promise;
  function allResolver(value, i) {
    resolve(value).then(resolveFromAll, function (error) {
      if (!called) {
        called = true;
        handlers.reject(promise, error);
      }
    });
    function resolveFromAll(outValue) {
      values[i] = outValue;
      if (++resolved === len & !called) {
        called = true;
        handlers.resolve(promise, values);
      }
    }
  }
}
},{"./INTERNAL":1,"./handlers":3,"./promise":6,"./reject":9,"./resolve":10}],3:[function(_dereq_,module,exports){
'use strict';
var tryCatch = _dereq_('./tryCatch');
var resolveThenable = _dereq_('./resolveThenable');
var states = _dereq_('./states');

exports.resolve = function (self, value) {
  var result = tryCatch(getThen, value);
  if (result.status === 'error') {
    return exports.reject(self, result.value);
  }
  var thenable = result.value;

  if (thenable) {
    resolveThenable.safely(self, thenable);
  } else {
    self.state = states.FULFILLED;
    self.outcome = value;
    var i = -1;
    var len = self.queue.length;
    while (++i < len) {
      self.queue[i].callFulfilled(value);
    }
  }
  return self;
};
exports.reject = function (self, error) {
  self.state = states.REJECTED;
  self.outcome = error;
  var i = -1;
  var len = self.queue.length;
  while (++i < len) {
    self.queue[i].callRejected(error);
  }
  return self;
};

function getThen(obj) {
  // Make sure we only access the accessor once as required by the spec
  var then = obj && obj.then;
  if (obj && typeof obj === 'object' && typeof then === 'function') {
    return function appyThen() {
      then.apply(obj, arguments);
    };
  }
}
},{"./resolveThenable":11,"./states":12,"./tryCatch":13}],4:[function(_dereq_,module,exports){
module.exports = exports = _dereq_('./promise');

exports.resolve = _dereq_('./resolve');
exports.reject = _dereq_('./reject');
exports.all = _dereq_('./all');
exports.race = _dereq_('./race');
},{"./all":2,"./promise":6,"./race":8,"./reject":9,"./resolve":10}],5:[function(_dereq_,module,exports){
(function (global){
'use strict';
if (typeof global.Promise !== 'function') {
  global.Promise = _dereq_('./index');
}
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./index":4}],6:[function(_dereq_,module,exports){
'use strict';

var unwrap = _dereq_('./unwrap');
var INTERNAL = _dereq_('./INTERNAL');
var resolveThenable = _dereq_('./resolveThenable');
var states = _dereq_('./states');
var QueueItem = _dereq_('./queueItem');

module.exports = Promise;
function Promise(resolver) {
  if (!(this instanceof Promise)) {
    return new Promise(resolver);
  }
  if (typeof resolver !== 'function') {
    throw new TypeError('resolver must be a function');
  }
  this.state = states.PENDING;
  this.queue = [];
  this.outcome = void 0;
  if (resolver !== INTERNAL) {
    resolveThenable.safely(this, resolver);
  }
}

Promise.prototype['catch'] = function (onRejected) {
  return this.then(null, onRejected);
};
Promise.prototype.then = function (onFulfilled, onRejected) {
  if (typeof onFulfilled !== 'function' && this.state === states.FULFILLED ||
    typeof onRejected !== 'function' && this.state === states.REJECTED) {
    return this;
  }
  var promise = new Promise(INTERNAL);


  if (this.state !== states.PENDING) {
    var resolver = this.state === states.FULFILLED ? onFulfilled: onRejected;
    unwrap(promise, resolver, this.outcome);
  } else {
    this.queue.push(new QueueItem(promise, onFulfilled, onRejected));
  }

  return promise;
};

},{"./INTERNAL":1,"./queueItem":7,"./resolveThenable":11,"./states":12,"./unwrap":14}],7:[function(_dereq_,module,exports){
'use strict';
var handlers = _dereq_('./handlers');
var unwrap = _dereq_('./unwrap');

module.exports = QueueItem;
function QueueItem(promise, onFulfilled, onRejected) {
  this.promise = promise;
  if (typeof onFulfilled === 'function') {
    this.onFulfilled = onFulfilled;
    this.callFulfilled = this.otherCallFulfilled;
  }
  if (typeof onRejected === 'function') {
    this.onRejected = onRejected;
    this.callRejected = this.otherCallRejected;
  }
}
QueueItem.prototype.callFulfilled = function (value) {
  handlers.resolve(this.promise, value);
};
QueueItem.prototype.otherCallFulfilled = function (value) {
  unwrap(this.promise, this.onFulfilled, value);
};
QueueItem.prototype.callRejected = function (value) {
  handlers.reject(this.promise, value);
};
QueueItem.prototype.otherCallRejected = function (value) {
  unwrap(this.promise, this.onRejected, value);
};
},{"./handlers":3,"./unwrap":14}],8:[function(_dereq_,module,exports){
'use strict';
var Promise = _dereq_('./promise');
var reject = _dereq_('./reject');
var resolve = _dereq_('./resolve');
var INTERNAL = _dereq_('./INTERNAL');
var handlers = _dereq_('./handlers');
module.exports = race;
function race(iterable) {
  if (Object.prototype.toString.call(iterable) !== '[object Array]') {
    return reject(new TypeError('must be an array'));
  }

  var len = iterable.length;
  var called = false;
  if (!len) {
    return resolve([]);
  }

  var resolved = 0;
  var i = -1;
  var promise = new Promise(INTERNAL);

  while (++i < len) {
    resolver(iterable[i]);
  }
  return promise;
  function resolver(value) {
    resolve(value).then(function (response) {
      if (!called) {
        called = true;
        handlers.resolve(promise, response);
      }
    }, function (error) {
      if (!called) {
        called = true;
        handlers.reject(promise, error);
      }
    });
  }
}
},{"./INTERNAL":1,"./handlers":3,"./promise":6,"./reject":9,"./resolve":10}],9:[function(_dereq_,module,exports){
'use strict';

var Promise = _dereq_('./promise');
var INTERNAL = _dereq_('./INTERNAL');
var handlers = _dereq_('./handlers');
module.exports = reject;

function reject(reason) {
	var promise = new Promise(INTERNAL);
	return handlers.reject(promise, reason);
}
},{"./INTERNAL":1,"./handlers":3,"./promise":6}],10:[function(_dereq_,module,exports){
'use strict';

var Promise = _dereq_('./promise');
var INTERNAL = _dereq_('./INTERNAL');
var handlers = _dereq_('./handlers');
module.exports = resolve;

var FALSE = handlers.resolve(new Promise(INTERNAL), false);
var NULL = handlers.resolve(new Promise(INTERNAL), null);
var UNDEFINED = handlers.resolve(new Promise(INTERNAL), void 0);
var ZERO = handlers.resolve(new Promise(INTERNAL), 0);
var EMPTYSTRING = handlers.resolve(new Promise(INTERNAL), '');

function resolve(value) {
  if (value) {
    if (value instanceof Promise) {
      return value;
    }
    return handlers.resolve(new Promise(INTERNAL), value);
  }
  var valueType = typeof value;
  switch (valueType) {
    case 'boolean':
      return FALSE;
    case 'undefined':
      return UNDEFINED;
    case 'object':
      return NULL;
    case 'number':
      return ZERO;
    case 'string':
      return EMPTYSTRING;
  }
}
},{"./INTERNAL":1,"./handlers":3,"./promise":6}],11:[function(_dereq_,module,exports){
'use strict';
var handlers = _dereq_('./handlers');
var tryCatch = _dereq_('./tryCatch');
function safelyResolveThenable(self, thenable) {
  // Either fulfill, reject or reject with error
  var called = false;
  function onError(value) {
    if (called) {
      return;
    }
    called = true;
    handlers.reject(self, value);
  }

  function onSuccess(value) {
    if (called) {
      return;
    }
    called = true;
    handlers.resolve(self, value);
  }

  function tryToUnwrap() {
    thenable(onSuccess, onError);
  }

  var result = tryCatch(tryToUnwrap);
  if (result.status === 'error') {
    onError(result.value);
  }
}
exports.safely = safelyResolveThenable;
},{"./handlers":3,"./tryCatch":13}],12:[function(_dereq_,module,exports){
// Lazy man's symbols for states

exports.REJECTED = ['REJECTED'];
exports.FULFILLED = ['FULFILLED'];
exports.PENDING = ['PENDING'];
},{}],13:[function(_dereq_,module,exports){
'use strict';

module.exports = tryCatch;

function tryCatch(func, value) {
  var out = {};
  try {
    out.value = func(value);
    out.status = 'success';
  } catch (e) {
    out.status = 'error';
    out.value = e;
  }
  return out;
}
},{}],14:[function(_dereq_,module,exports){
'use strict';

var immediate = _dereq_('immediate');
var handlers = _dereq_('./handlers');
module.exports = unwrap;

function unwrap(promise, func, value) {
  immediate(function () {
    var returnValue;
    try {
      returnValue = func(value);
    } catch (e) {
      return handlers.reject(promise, e);
    }
    if (returnValue === promise) {
      handlers.reject(promise, new TypeError('Cannot resolve promise with itself'));
    } else {
      handlers.resolve(promise, returnValue);
    }
  });
}
},{"./handlers":3,"immediate":16}],15:[function(_dereq_,module,exports){

},{}],16:[function(_dereq_,module,exports){
'use strict';
var types = [
  _dereq_('./nextTick'),
  _dereq_('./mutation.js'),
  _dereq_('./messageChannel'),
  _dereq_('./stateChange'),
  _dereq_('./timeout')
];
var draining;
var queue = [];
function drainQueue() {
  draining = true;
  var i, oldQueue;
  var len = queue.length;
  while (len) {
    oldQueue = queue;
    queue = [];
    i = -1;
    while (++i < len) {
      oldQueue[i]();
    }
    len = queue.length;
  }
  draining = false;
}
var scheduleDrain;
var i = -1;
var len = types.length;
while (++ i < len) {
  if (types[i] && types[i].test && types[i].test()) {
    scheduleDrain = types[i].install(drainQueue);
    break;
  }
}
module.exports = immediate;
function immediate(task) {
  if (queue.push(task) === 1 && !draining) {
    scheduleDrain();
  }
}
},{"./messageChannel":17,"./mutation.js":18,"./nextTick":15,"./stateChange":19,"./timeout":20}],17:[function(_dereq_,module,exports){
(function (global){
'use strict';

exports.test = function () {
  if (global.setImmediate) {
    // we can only get here in IE10
    // which doesn't handel postMessage well
    return false;
  }
  return typeof global.MessageChannel !== 'undefined';
};

exports.install = function (func) {
  var channel = new global.MessageChannel();
  channel.port1.onmessage = func;
  return function () {
    channel.port2.postMessage(0);
  };
};
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],18:[function(_dereq_,module,exports){
(function (global){
'use strict';
//based off rsvp https://github.com/tildeio/rsvp.js
//license https://github.com/tildeio/rsvp.js/blob/master/LICENSE
//https://github.com/tildeio/rsvp.js/blob/master/lib/rsvp/asap.js

var Mutation = global.MutationObserver || global.WebKitMutationObserver;

exports.test = function () {
  return Mutation;
};

exports.install = function (handle) {
  var called = 0;
  var observer = new Mutation(handle);
  var element = global.document.createTextNode('');
  observer.observe(element, {
    characterData: true
  });
  return function () {
    element.data = (called = ++called % 2);
  };
};
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],19:[function(_dereq_,module,exports){
(function (global){
'use strict';

exports.test = function () {
  return 'document' in global && 'onreadystatechange' in global.document.createElement('script');
};

exports.install = function (handle) {
  return function () {

    // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
    // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
    var scriptEl = global.document.createElement('script');
    scriptEl.onreadystatechange = function () {
      handle();

      scriptEl.onreadystatechange = null;
      scriptEl.parentNode.removeChild(scriptEl);
      scriptEl = null;
    };
    global.document.documentElement.appendChild(scriptEl);

    return handle;
  };
};
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],20:[function(_dereq_,module,exports){
'use strict';
exports.test = function () {
  return true;
};

exports.install = function (t) {
  return function () {
    setTimeout(t, 0);
  };
};
},{}]},{},[5]);

/**
 * Console-polyfill. MIT license.
 * @author paulmillr
 * @link https://github.com/paulmillr/console-polyfill
 */
(function(global) {
  'use strict';
  global.console = global.console || {};
  var con = global.console;
  var prop, method;
  var empty = {};
  var dummy = function() {};
  var properties = 'memory'.split(',');
  var methods = ('assert,clear,count,debug,dir,dirxml,error,exception,group,' +
     'groupCollapsed,groupEnd,info,log,markTimeline,profile,profiles,profileEnd,' +
     'show,table,time,timeEnd,timeline,timelineEnd,timeStamp,trace,warn').split(',');
  while (prop = properties.pop()) if (!con[prop]) con[prop] = empty;
  while (method = methods.pop()) if (!con[method]) con[method] = dummy;
})(typeof window === 'undefined' ? this : window);

window.Invodo = window.Invodo || {};

window.Invodo.Ixd = window.Invodo.Ixd || {};

(function(namespace) {
  'use strict';

'use strict';

// Utility functions used throughout IVP
var Utility = (function() {

  var util = {
    /**
     * get a unique id
     * @returns {number} uniqueId
     */
    _uniqueId: 1,
    uniqueId: function() {
      return Utility._uniqueId++;
    },
    getParameterByName: function(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    /**
    * Test for HTML5 video support
    * @returns {Boolean}
    */
    supportsVideo: function() {
      return !!document.createElement('video').canPlayType;
    },
    /**
    * extend object.
    * means that properties in dest will be overwritten by the ones in src.
    * @param {Object} dest
    * @param {Object} src
    * @param {Boolean} [merge]
    * @returns {Object} dest
    */
    extend: function(dest, src, merge) {
      var keys = Object.keys(src);
      var i = 0;
      while (i < keys.length) {
        if (!merge || (merge && dest[keys[i]] === undefined)) {
          dest[keys[i]] = src[keys[i]];
        }
        i++;
      }
      return dest;
    },
    /**
    * merge the values from src in the dest.
    * means that properties that exist in dest will not be overwritten by src
    * @param {Object} dest
    * @param {Object} src
    * @returns {Object} dest
    */
    merge: function(dest, src) {
      return Utility.extend(dest, src, true);
    },
    /**
    * Create a random Widget name by generate a random key.
    * means the length specified will decrease the chance of repeating random names
    * @param {Integer} length
    * @returns {String} text
    */
    randString: function(size) {
      var text = '';
      var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      for (var i = 0; i < size; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    },
    /**
     * simple function bind
     * @param {Function} fn
     * @param {Object} context
     * @returns {Function}
    */
    bindFn: function(fn, context) {
      return function boundFn() {
        return fn.apply(context, arguments);
      };
    },
    /**
     * set a timeout with a given scope
     * @param {Function} fn
     * @param {Number} timeout
     * @param {Object} context
     * @returns {number}
    */
    setTimeoutContext: function(fn, timeout, context) {
      return setTimeout(Utility.bindFn(fn, context), timeout);
    },
    /**
     * @TODO Document this function
    */
    throttle: function(func, interval) {
      var lastCall = 0;
      return function() {
        var now = Date.now();
        if (lastCall + interval < now) {
          lastCall = now;
          return func.apply(this, arguments);
        }
      };
    },
    /**
     * @TODO Document this function
    */
    formatTime: function(time) {
      var h = parseInt(time / 3600) % 24;
      var m = parseInt(time / 60) % 60;
      var s = time % 60;
      if (h < 1) {
        // under an hour
        return m + ':' + (s < 10 ? '0' + s : s);
      }
      // over an hour
      return h + ':' + (m < 10 ? '0' + m : m) + ':' + (s  < 10 ? '0' + s : s);
    },
    /**
    * @TODO Document this function
    */
    mixin: function(target, source, props) {
      if (props) {
        if (props instanceof Array) {
          for (var i = 0; i < props.length; i++) {
            target[props[i]] = source[props[i]];
          }
        } else if (source[props]) {
          target[props] = source[props];
        }
      } else {
        for (var prop in source) {
          target[prop] = source[prop];
        }
      }
    },
    /**
     * Creates a stylesheet which can be used to store dynamic styles
     * The stylesheet is also added to the document's head
     * This is useful because we can avoid writing CSS inline
     * @returns {Object} styleSheet
    **/
    createStyleSheet: function() {
      // Create the <style> tag
      var style = document.createElement('style');
      // WebKit hack :(
      style.appendChild(document.createTextNode(''));
      // Add the <style> element to the page
      document.head.appendChild(style);

      return style.sheet;
    },
    /**
     * Add a CSS rule to a stylesheet
     * @param {Object} styleSheet
     * @param {String} selector
     * @param {String} rules
     * @param {Boolean} index
     * @usage Utility.addCSSRule(styleSheet, '#great-div', 'padding-bottom: 50px')
    **/
    addCSSRule: function(sheet, selector, rules, index) {
      if ('insertRule' in sheet) {
        sheet.insertRule(selector + '{' + rules + '}', index);
      } else if ('addRule' in sheet) {
        sheet.addRule(selector, rules, index);
      }
    },
    /**
     * Find an element parent via event target
     * @param {String} className
     * @param {Event Object} event
     * @usage Utility.findParent('ivp-hotspot', originalClickEvent)
    **/
    findParent: function(className, event) {
      // Make sure we are working on the outermost element
      var target = event.target || event.srcElement;
      while (!target.classList.contains(className)) {
        target = target.parentNode;
      }
      return target;
    }
  };

  return util;

})();

// Element utilities - eventually should be rolled into Utility namespace
var Elem = (function() {
  var el = {
    container: function(id, name, className) {
      var el = Elem.add('div', className);
      el.id = name + '-' + id;
      return el;
    },
    add: function(type, name, custom) {
      var el = document.createElement(type);
      if (name) {
        el.className = name;
      }
      if (custom) {
        el.className = el.className + ' ' + custom;
      }
      return el;
    },
    button: function(text, className, customClass) {
      var el = Elem.add('div', className, customClass);
      el.setAttribute('role', 'button');

      var d = document.createElement('div');
      var s = document.createElement('span');

      s.innerHTML = text;

      d.appendChild(s);
      el.appendChild(d);

      return el;
    }
  };
  return el;

})();

function extend(dest, src, merge) {
  var keys = Object.keys(src);
  var i = 0;
  while (i < keys.length) {
    if (!merge || (merge && dest[keys[i]] === undefined)) {
      dest[keys[i]] = src[keys[i]];
    }
    i++;
  }
  return dest;
}

/**
 * addEventListener with multiple events at once
 * @param {EventTarget} target
 * @param {String} types
 * @param {Function} handler
 */
function addEventListeners(target, types, handler) {
  _.forEach(types.split(' '), function(type) {
    target.addEventListener(type, handler, false);
  });
}

/**
 * removeEventListener with multiple events at once
 * @param {EventTarget} target
 * @param {String} types
 * @param {Function} handler
 */
function removeEventListeners(target, types, handler) {
  _.forEach(types.split(' '), function(type) {
    target.removeEventListener(type, handler, false);
  });
}


// A-> $http function is implemented in order to follow the standard Adapter pattern
function $http(url){

  // A small example of object
  var core = {

    // Method that performs the ajax request
    ajax : function (method, url, args) {

      // Creating a promise
      var promise = new Promise( function (resolve, reject) {

        // Instantiates the XMLHttpRequest
        var client = new XMLHttpRequest();
        var uri = url;

        if (args && (method === 'POST' || method === 'PUT')) {
          uri += '?';
          var argcount = 0;
          for (var key in args) {
            if (args.hasOwnProperty(key)) {
              if (argcount++) {
                uri += '&';
              }
              uri += encodeURIComponent(key) + '=' + encodeURIComponent(args[key]);
            }
          }
        }

        client.open(method, uri, true);
        client.send();

        client.onload = function () {
          if (this.status == 200) {
            // Performs the function "resolve" when this.status is equal to 200
            // responseText is the old-school way of retrieving response (supported by IE8 & 9)
            // response/responseType properties were introduced in XHR Level2 spec (supported by IE10)
            var response = ('response' in this) ? this.response : this.responseText;
            resolve(response);
          } else {
            // Performs the function "reject" when this.status is different than 200
            reject(this.statusText);
          }
        };
        client.onerror = function () {
          reject(this.statusText);
        };
      });

      // Return the promise
      return promise;
    }
  };

  // Adapter pattern
  return {
    'get' : function(args) {
      return core.ajax('GET', url, args);
    },
    'post' : function(args) {
      return core.ajax('POST', url, args);
    },
    'put' : function(args) {
      return core.ajax('PUT', url, args);
    },
    'delete' : function(args) {
      return core.ajax('DELETE', url, args);
    }
  };
};
// End A

/* global Ivp */
'use strict';

function ElementInterface(data) {
  // @TODO Error handling
  // Store the data items
  this._items = (data.hasOwnProperty('items')) ? data.items : {};
  // Setup element holder so we don't have to check its existence all day
  this._elements = [];
  return this;
}

ElementInterface.prototype = {
  /**
   * Get Item data by Id - items are HotSpot or Card obj
   * @param {Integer || String} id
   */
  getItemById: function(id) {
    // Make sure we are passing something safe into parseInt
    if (_.isNull(id) || _.isPlainObject(id) || _.isFunction(id)) {
      return;
    }
    // Should check if this._templates exists if not create it and return false
    var index = _.findIndex(this._items, {id: parseInt(id)});
    // return the template or undefined
    return (index > -1) ? this._items[index] : false;
  },
  /**
   * Add an Element to the registry
   * @param {HTMLElement} element
   */
  addElement: function(element) {
    if (!_.isElement(element)) {
      return false;
    }
    this._elements.push(element);
    return element;
  },
  /**
   * Get a Element by it's ID - or data-id more precisely
   * @param {Integer} id
   * @return {HTMLElement}
   */
  getElement: function(id) {
    if (_.isUndefined(id)) {
      return;
    }
    id = parseInt(id);
    var index = _.findIndex(this._elements, function(el) {
      return parseInt(el.dataset.id) === id;
    });
    // return the element or false
    return (index > -1) ? this._elements[index] : false;
  },
  /**
   * Get all the elements
   * @return [cards]{HTMLElement}
   */
  getElements: function() {
    return this._elements;
  }
};

// I provide an interface with dealing with item templates
// I am used by Cards HotSpots
function Templater(data) {
  // @TODO Error handling
  this._templates = (data.hasOwnProperty('templates')) ? data.templates : {};
  return this;
}

Templater.prototype = {
  /**
   * Add a template
   * @param {Object} template
   */
  addTemplate: function(template) {
    // Check for template format errors
    if (!_.isPlainObject(template) || !template.hasOwnProperty('id') || !template.hasOwnProperty('text')) {
      return;
    }
    return this._templates.push(template);
  },
  /**
   * Get a template object by passing a data object like a Card or HotSpot
   * @param {Object} card
   */
  getTemplate: function(item) {
    // Must pass a plain object and it must have a template property
    if (!_.isPlainObject(item) || !item.hasOwnProperty('template')) {
      return;
    }
    var id = parseInt(item.template);
    return this.getTemplateById(id);
  },
  /**
   * Get a data object template by passing an id
   * @param {Object} card
   */
  getTemplateById: function(id) {
    // Make sure we are passing something safe into parseInt
    if (_.isNull(id) || _.isPlainObject(id) || _.isFunction(id)) {
      return;
    }
    var index = _.findIndex(this._templates, {id: parseInt(id)});
    return (index > -1) ? this._templates[index] : false;
  },
  /**
   * Get all active templates
   */
  getTemplates: function() {
    return this._templates;
  },
  /**
   * Render the template
   * @param {Object} template
   * @returns {String}
   */
  renderTemplate: function(obj) {
    var template = this.getTemplateById(obj.template);
    // using the ES delimiter as an alternative to the default "interpolate" delimiter
    var compiled = _.template(template.text);
    return compiled(obj.data);
  },
  /**
   * Get the default template
   * @param {String} type - type of data object, card or hotspot
   * @TODO This feature needs to be created
   */
  getDefaultTemplate: function(type) {
    var template;
    switch (type) {
      case 'hotspot':
        template = Ivp.defaults.hotspots.theme;
        break;
      case 'card':
        template = Ivp.defaults.cards.theme;
        break;
      default:
        template = '';
        break;
    }

    return {
      id: 0,
      text: template
    };
  }
};

/* global Utility, Elem, extend */
'use strict';

function Screen(element, widget, eventManager, data) {
  // data is optional at first
  data = data || {};
  // Store the event manager
  this.observer = eventManager;
  // Add this component to the event manager event response
  // this.observer.addComponent(this);
  // We need the template interface
  Templater.call(this, data);
  // Run it!
  this.init(element, widget, data);
}

Screen.prototype = {

  update: function(data) {
    // need data to update the poster
    if (!_.isPlainObject(data)) {
      return;
    }
    Templater.call(this, data);
    // poster data comes in as an array, not sure why that is...
    // it is so we can have multiple posters. but that feature
    // does no currently exists - so just grab the first item
    var obj = data.items[0];
    // Create each poster - currently support only one poster
    var poster = this.create(obj);
    // Delete all the posters
    while (this._parent.lastChild) {
      this._parent.removeChild(this._parent.lastChild);
    }
    // Add the poster to the parent node
    this._parent.appendChild(poster);
  },

  enable: function() {
    this._parent.classList.remove('ivp-hide');
    this._widget.classList.add('ivp-player-off');
  },

  disable: function() {
    this._parent.classList.add('ivp-hide');
    this._widget.classList.remove('ivp-player-off');
  },

  onClick: function(event) {
    event.stopPropagation();
    // form the event response
    var resp = extend(this.observer.update(), {
      type: 'screen.click',
      srcEvent: event
    });
    // Trigger the posterclick event
    this.observer.trigger('screen.click', resp);
    // let it go
    resp = null;
    return false;
  },

  create: function(poster) {
    var el = Elem.add('div', 'ivp-screen');
    // Render the template and add the content
    el.innerHTML = this.renderTemplate(poster);
    // Add the ID
    el.setAttribute('data-id', poster.id);

    return el;
  },

  eventFilter: function(event) {
    return event;
  },

  init: function(element, widget, data) {
    // Create Card container
    this._parent = element;
    this._widget = widget;
    if (data.hasOwnProperty('items') && data.items.length > 0) {
      // Create each poster - currently support only one poster
      var node = this.create(data.items[0]);
      // Add the poster to the parent node
      this._parent.appendChild(node);
    }
    // store scope ref for addEventListener
    // var _this = this;
    // Add a default event handler to the Poster
    // this._parent.addEventListener('click', function(event) {
    //   _this.onClick.call(_this, event);
    // }, false);
  }

};

// Add the interfaces
Utility.mixin(Screen.prototype, Templater.prototype);

/* global Utility, extend */
'use strict';

// We no longer need to create a stylesheet - do that here for inplayer support
// Create a stylesheet to store all widget specific styles
// settings.stylesheet = Utility.createStyleSheet();


// This is an opinionated wrapper of Invodo.Widget.add
function Video(settings, observer, callback) {
  // Create empty pod video config
  var config = {};
  // Store the event bus
  this.observer = observer;
  // Add this component to the event manager event response
  this.observer.addComponent(this);
  // Store the duration - hack - this lives outside InPlayer
  this._duration = settings.data.video.duration;
  // Store the last timeupdate
  this._lastTimeUpdate = 0;
  // Store if the player hasPlayed
  this._hasPlay = false;
  // Store if the player is able to play
  this._canPlay = false;
  // Reliable playstate
  this._playState = false;
  // Reliable ended state
  this._ended = false;
  // Return the video pod config
  this.getConfig = function() {
    return config;
  };
  // Save the video pod config
  this.setConfig = function(podID) {
    config = Invodo.Pod.get(podID);
  };
  // Create the widget
  // Store the reference
  var _this = this;
  _this.init(settings, function(id) {

    // Store the pod config
    _this.setConfig(id);
    // take a look at config.loaded to make sure it is real
    var eventData = {
      mpd: config.mpd,
      pod:config.pod,
      fr: config.config.frames[0].durableId,
      duration: this.getDuration(),
      ex: 'null/' + config.pod
    };
    // Send config data to event manager
    _this.observer.set(eventData);
    // Size the widget
    _this.size(settings.stylesheet, '#' + settings._el.video.id);
    // Register the events to the widget
    _this.addEvents();
    // Fire on callbacks, the video widget is ready to be used
    callback.call(_this);
  });

}

// Use the interface
Video.prototype = {
  /**
   * InPlayer timeUpdate Event Handler
   * @param {Object} event
   */
  _onTimeUpdate: function(event) {
    // sometimes timeUpdate returns without a 'position' property
    // these events should be ignored because they are not useful.
    if (!event.position) {
      return;
    }
    // timeUpdate should only fire if the video is currently playing
    // If the video is paused or stopped timeUpdate should never fire
    if (this._playState === false) {
      return;
    }
    // Duplicate events do not report them
    // @TODO may be able to use == here instead of converting both the numbers to string
    if (event.position.toString() === this._lastTimeUpdate.toString()) {
      return;
    }
    // Store the last 'position' (number) instead of the whole object
    // the position property is the only important property that updates
    // It is more reasonable to compare two strings, than two objects
    this._lastTimeUpdate = event.position;
    // form the event response
    var resp = extend(this.observer.update(), {
      type: 'timeupdate',
      // @TODO deprecate this key - it is used internally by controls
      position: event.position,
      time: event.position,
      duration: event.duration,
      srcEvent: event
    });
    // trigger the timeupdate event
    this.observer.trigger('timeupdate', resp);
    resp = null;
  },
  /**
   * InPlayer podStart Event Handler
   * @param {Object} event
   */
  _onPodStart: function(event) {
    // the video has now been played
    this._hasPlay = true;
    // the video is now playing
    this._playState = true;
    // Form the event response
    var resp = extend(this.observer.update(), {
      type: 'firstplay',
      srcEvent: event
    });
    // Trigger the firstplay event
    this.observer.trigger('firstplay', resp);
    // Pass the event to the play event handler
    this._onPlay(event);
    resp = null;
  },
  /**
   * InPlayer videoPause Event Handler
   * @param {Object} event
   */
  _onPause: function(event) {
    // the video is now paused
    this._playState = false;
    // Form the event response
    var resp = extend(this.observer.update(), {
      type: 'pause',
      srcEvent: event
    });
    // trigger the pause event
    this.observer.trigger('pause', resp);
    resp = null;
  },
  /**
   * InPlayer videoUnpause Event Handler
   * @param {Object} event
   */
  _onPlay: function(event) {
    this._playState = true;
    // Form the event response
    var resp = extend(this.observer.update(), {
      type: 'play',
      srcEvent: event
    });
    this.observer.trigger('play', resp);
    resp = null;
  },
  /**
   * InPlayer playerReady Event Handler
   * @param {Object} event
   */
  _onReady: function(event) {
    // the video can now be played
    this._canPlay = true;
    // form the event response
    var resp = extend(this.observer.update(), {
      type: 'playerReady',
      srcEvent: event
    });
    // trigger an alias playerReady event
    this.observer.trigger('ready', resp);
    // trigger the playerReady event
    this.observer.trigger('playerReady', resp);
    resp = null;
  },
  /**
   * InPlayer videoComplete Event Handler
   * @param {Object} event
   */
  _onComplete: function(event) {
    // the video cannot be played because it is over
    this._canPlay = false;
    // the video is not playing
    this._playState = false;
    // the video has ended
    this._ended = true;
    // form the event response
    var resp = extend(this.observer.update(), {
      type: 'ended',
      srcEvent: event
    });
    // trigger the ended event
    this.observer.trigger('ended', resp);
    // resp = null;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns extended event response
   */
  eventFilter: function(event) {
    // Update the event cache
    return extend(event, {
      hasplay: this._hasPlay,
      canplay: this._canPlay,
      playing: this._playState,
      paused: !this._playState,
      time: this._lastTimeUpdate,
      duration: this._duration,
      ended: this._ended
    });
  },
  /**
   * Get the video duration
   * @returns duration
   */
  duration: function() {
    // before inplayer is played the getDuration method returns false
    // we store the video duration inside the settings config as a work around
    // after the video has played it would be best to get duration from the pod
    if (this._hasPlay) {
      this._duration = this._widget.getDuration();
    }
    return this._duration;
  },
  /**
   * Conditional to check if the video is playing
   * @returns boolean
   */
  isPlaying: function() {
    return this._playState;
  },
  /**
   * Conditional to check if the video is paused
   * @returns boolean
   */
  isPaused: function() {
    return !this._playState;
  },
  /**
   * Size the player container based off the video aspect ratio
   * @param {Object} stylesheet
   * @param {Element} element
   */
  size: function(stylesheet, element) {
    // Get the video config obj
    var config = this.getConfig();
    // Super fragile, but we are getting the first frame and it's first encoding
    var active = config.config.frames[0].encodings[0];
    // Store the video height
    this._height = active.height;
    // Store the video width
    this._width = active.width;
    // Store the video aspect ratio
    this._ratio = active.height / active.width;
    // Write the rule
    Utility.addCSSRule(stylesheet, element, 'padding-bottom: ' + (this._ratio * 100) + '%');
  },
  /**
   * Bind to all the InPlayer events
   */
  addEvents: function() {
    // Bind podStart event
    this._widget.registerEventListener('podStart', this._onPodStart.bind(this));
    // Bind videoUnpause event
    this._widget.registerEventListener('videoUnpause', this._onPlay.bind(this));
    // Bind videoPause event
    this._widget.registerEventListener('videoPause', this._onPause.bind(this));
    // Bind timeUpdate event
    this._widget.registerEventListener('timeUpdate', this._onTimeUpdate.bind(this));
    // Bind playerReady event
    this._widget.registerEventListener('playerReady', this._onReady.bind(this));
    // Bind playerReady event
    this._widget.registerEventListener('videoComplete', this._onComplete.bind(this));
  },
  // The current implementation of 'unregisterEventListener' does not work
  // consistently across each event type. I imagine this will be addressed
  // soon. When this is addressed I will implement removeEvents().
  removeEvents: function() {
    return;
  },
  /**
   * Create the widget and fire a callback when it is ready
   * @param {Object} settings
   * @param {Function} callback
   */
  init: function(settings, callback) {
    // Clone the refID from the config
    settings._inplayer.refId = settings.refId;
    // Create a unique ID by referencing the domID
    settings._el.video.id = 'ivp-inplayer-container-' + settings.domID;
    // Invodo.Widget.add requires the parentDomId to be a string :(
    settings._inplayer.parentDomId = settings._el.video.id;
    // Really do not like this setting crap here
    // Set a callback to save the podID
    settings._inplayer.onpodload = callback;
    // Create the Inplayer widget
    this._widget = Invodo.Widget.add(settings._inplayer);
  }
};

/* global extend */
'use strict';

// Player is an interface for interacting with Video and its Inplayer widget
function Player(observer) {
  // Reliable playback checker
  this.hasPlayed = false;
  // Create container for video
  this._video = false;
  // Store the player mute state
  this._muted = false;
  // Events d00d
  this.observer = observer;
  // Add this component to the event manager event response
  this.observer.addComponent(this);
}

Player.prototype = {
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  eventFilter: function(event) {
    // Update the event cache
    return extend(event, {
      muted: this._muted,
      volume: (this._muted) ? 0 : 10
    });
  },
  // Store a video
  set: function(video) {
    if (this._video) {
      this.remove();
    }
    // Store the video widget
    this._video = video;
  },
  // if a video is already set - loop through its events and remove them
  // This will be implemented durning CYOA stories
  remove: function() {},
  // Play the video
  play: function() {
    // Get out of here if we are already playing
    if (this._video.isPlaying()) {
      return;
    }
    // @TODO Need to check to see if this issue was addressed in inPlayer
    // fixes ticket: https://jira.invodo.com/browse/UPF-957
    if (this._video._widget.isStopped() && this._video._widget.getTime() > 0) {
      this.reset(true);
    }
    this._video._widget.play();
    // set the duration
    // have to play inplayer once to get duration from the pod
    if (_.isNull(this._duration)) {
      this.duration();
    }
  },
  // Pause the video
  pause: function() {
    this._video._widget.pause();
  },
  // Get or set the video time
  currentTime: function(value) {
    // getting the current time
    if (_.isUndefined(value) || !_.isNumber(value)) {
      return this._video._widget.getTime();
    }
    this._video._widget.setTime(value);
  },
  // Stop the video
  stop: function() {
    this._video._widget.stop();
    // form the event response
    var resp = extend(this.observer.update(), {
      type: 'stop'
    });
    // trigger the stop event
    this.observer.trigger('stop', resp);
    resp = null;
  },
  // Toggle the video play state
  playToggle: function() {
    if (this._video.isPlaying()) {
      this.pause();
      return;
    }
    this.play();
  },
  // Get or set the video mute state
  mute: function(value) {
    this._muted = this._video._widget.isMuted();
    // If no values are passed then return the state
    if (_.isUndefined(value) || !_.isBoolean(value)) {
      return this._muted;
    }
    // If you set the same as the current value return
    if (value === this._muted) {
      return this._muted;
    }
    // Toggle the mute state
    this._video._widget.muteToggle();
    // Store the new mute state
    this._muted = value;
    // form the event response
    var resp = extend(this.observer.update(), {
      type: 'volumechange',
      // @TODO deprecate this key - controls depends on it
      state: (this._muted) ? 'mutedOn' : 'mutedOff',
      muted: this._muted,
      volume: (this._muted) ? 0 : 10
    });
    // trigger the volume change event
    this.observer.trigger('volumechange', resp);
    resp = null;
  },
  // Reset the video
  reset: function() {

    this.currentTime(0);
    this.pause();
    // // form the event response
    // var resp = extend(this.observer.update(), {
    //  type: 'reset'
    // });
    // this.observer.trigger('reset', resp);
    // resp = null;
  },
  // Replay the video
  replay: function() {
    this.currentTime(0);
    this.play();
    // form the event response
    var resp = extend(this.observer.update(), {
      type: 'replay'
    });
    this.observer.trigger('replay', resp);
  },
  // Get the video duration
  duration: function() {
    return this._video.duration();
  },
  // Check if the video is playing
  isPlaying: function() {
    return this._video.isPlaying();
  },
  // Check if the video is paused
  isPaused: function() {
    return this._video.isPaused();
  },
  // Check if the video is stopped
  isStopped: function() {
    return this._video._widget.isStopped();
  }
};

/* global extend */
'use strict';

var PlayerUtil = (function() {

  var api = {
    // Checks if the document is currently in fullscreen mode
    isFullScreen: function() {
      return !!(document.fullScreen || document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement || document.fullscreenElement);
    },
    canPlayType: function(video, type) {
      var canPlay = video.canPlayType(type);
      return ((canPlay === 'maybe') || (canPlay === 'probably'));
    },
    // create a video tag
    videoFactory: function(parent, poster, classname, id) {
      // Create a video element - that is what we do around here
      var video = document.createElement('video');
      // preload="auto"
      video.setAttribute('preload', 'auto');
      // add the poster attr
      video.poster = poster;
      // Do some more shit up here like add an id or class or whatever
      video.className = classname;
      // add the video id
      //video.id = 'ivp-video-' + id;
      // Set the data-id
      video.setAttribute('data-id', id);
      // go ahead and append the element
      parent.appendChild(video);
      // give it back
      return video;
    },
    // create a source tag
    sourceFactory: function(file, type) {
      // create the element
      var source = document.createElement('source');
      // set the src attribute
      source.setAttribute('src', file);
      // set the type attribute
      source.setAttribute('type', type);
      // log a warning if this source cannot be found
      source.addEventListener('error', function(e) {
        console.warn('A source file could not be loaded', e.target.src);
      });
      // give it back
      return source;
    },
    // watch the video tag for events
    videoWatcher: function(element, observer) {
      // the firstplay event
      var onetime = function(event) {
        // this is never happening again
        element.removeEventListener('play', onetime);
        // trigger the fake firstplay event
        observer.trigger('firstplay', extend(observer.update(), {
          type: 'firstplay',
          srcEvent: event
        }));
      };
      // handle each video event
      var handler = function(event){
        // form response
        var resp = extend(observer.update(), {
          type: event.type,
          srcEvent: event
        });
        //  trigger the event
        observer.trigger(event.type, resp);
        // let it go
        resp = null;
      };
      // https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Media_events
      var events = ['play', 'pause', 'ended', 'seeked', 'seeking', 'waiting', 'suspend', 'timeupdate', 'volumechange', 'loadedmetadata', 'stalled', 'progress', 'canplay'];
      _.forEach(events, function(event) {
        element.addEventListener(event, handler, false);
      });
      // video element does not provide a firstplay event
      element.addEventListener('play', onetime, false);
    }
  };

  // make it public
  return api;

})();


var frames = (function() {

  var api = {

    // search the collection
    findById: function(id, collection) {
      var index = _.findIndex(collection, {'id': id});
      return (index > -1) ? collection[index] : collection[0];
    },
    getDefault: function(config, defaultVideo) {
      // If there are not multiple clips get out
      if (!config.multiclip || !_.isString(defaultVideo)) {
        return config.items[0];
      }
      // It is safe to pass finById an empty string
      return frames.findById(defaultVideo, config.items);
    },
    // set the default encoding and handle
    getDefaultEncoding: function(encodings) {
      // If no default is found or there is only one
      if (!encodings.hasOwnProperty('default') || encodings.items.length === 0) {
        return encodings.items[0];
      }
      return frames.findById(encodings.default, encodings.items);
    },


  };

  return api;
})();


/**
 * HTML5 Video player
 * @param {DOMnode} parent
 * @param {Object} encodings
 * @param {String} poster
 * @param {EventManager} observer
 *
 */
function NativeVideo(parent, config, observer, defaultVideo) {
  // store all the frames
  this._frames = config.items;
  // set the default frame
  this._activeFrame = frames.getDefault(config, defaultVideo);
  // set the default encoding
  this._defaultEncoding = frames.getDefaultEncoding(this._activeFrame.encoding);
  // Store the observer
  this.observer = observer;
  // Add this component to the event manager event response
  this.observer.addComponent(this);
  // create the video element
  this._video = PlayerUtil.videoFactory(parent, this._activeFrame.poster, 'ivp-video', this._activeFrame.durableId);
  // register the events
  PlayerUtil.videoWatcher(this._video, this.observer);
  // Set the encoding
  this.setEncoding(this._defaultEncoding);
  // Simple check if the video has played
  this._hasPlay = !!this._video.played.length;
}

// Video API
NativeVideo.prototype = {
  frames: function() {
    return this._frames;
  },
  activeFrame: function() {
    return this._activeFrame;
  },
  get: function() {
    return this._video;
  },
  set: function(id) {
    var index = _.findIndex(this._frames, {'id': id});
    if (index < 0) {
      return;
    }
    // this.stop();

    this._activeFrame = this._frames[index];

    // set the default encoding
    this._defaultEncoding = frames.getDefaultEncoding(this._activeFrame.encoding);

    this.pause();
    this._video.poster = '';
    this._video.src = '';
    this._video.load();


    //this._video.id = 'ivp-video-' + this._activeFrame.id;
    // Set the data-id
    this._video.setAttribute('data-id', this._activeFrame.id);


    // create the video element
    //this._video = PlayerUtil.videoFactory(parent, this._activeFrame.poster, 'ivp-video', this._activeFrame.durableId);
    // Set the encoding
    this.setEncoding(this._defaultEncoding);
    // register the events
    //PlayerUtil.videoWatcher(this._video, this.observer);
    // this._video.load();
    this._video.poster = this._activeFrame.poster;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  setEncoding: function(obj) {
    // Make sure the player supports the codec
    if (PlayerUtil.canPlayType(this._video, obj.type)) {
      this._activeEncoding = obj;
      this._video.src = obj.src;
    }
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  eventFilter: function(event) {
    return extend(event, {
      time: this._video.currentTime,
      duration: this._video.duration,
      muted: this._video.muted,
      volume: this._video.volume,
      playing: !this._video.paused,
      paused: this._video.paused,
      fullscreen: PlayerUtil.isFullScreen(),
      ended: this._video.ended,
      hasplay: this._hasPlay,
      canplay: this._video.readyState,
      fr: this._video.dataset.id
    });
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  play: function() {
    if (!this._hasPlay) {
      this._hasPlay = true;
    }
    this._video.play();
    return this;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  playToggle: function() {
    if (this._video.paused) {
      this._video.play();
    } else {
      this._video.pause();
    }
    return this;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  pause: function() {
    this._video.pause();
    return this;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  // convience method - does not report
  stop: function() {
    this._video.pause();
    this.currentTime(0);
    return this;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  // convience method - does not report
  reset: function() {
    this.stop();
    return this;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  // convience method - does not report
  replay: function() {
    this.currentTime(0);
    this.play();
    return this;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  currentTime: function(value) {
    // need a number to do any setting
    if (!_.isNumber(value)) {
      return this._video.currentTime;
    }
    // if there is no change do not edit
    if (value === this._video.currentTime) {
      return this._video.currentTime;
    }
    // set the current time
    this._video.currentTime = value;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  duration: function() {
    return this._video.duration;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  mute: function(value) {
    // If no values are passed then return the state
    if (_.isUndefined(value) || !_.isBoolean(value)) {
      return this._video.muted;
    }
    this._video.muted = value;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  hasPlayed: function() {
    return this._hasPlay;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  isPlaying: function() {
    return !this._video.paused;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  isPaused: function() {
    return this._video.paused;
  }
};

/* global TimelineLite */
/* exported Timeline */
'use strict';

function Timeline(observer) {
  // Create a Greensock TimelineLite instance
  var time = new TimelineLite({paused: true});

  // Create handlers
  var handle = {
    // Event listener for player.play
    onPlay: function(event) {
      time.time(event.time);
      time.play();
    },
    // Event listener for player.pause
    onPause: function(event) {
      time.pause();
      time.time(event.time);
    },
    // Event listener for player.currentTime
    onSeeked: function(event) {
      time.time(event.time);
    }
  };

  observer.on('play', handle.onPlay);
  observer.on('pause', handle.onPause);
  observer.on('seeked', handle.onSeeked);

  this.get = function() {
    return time;
  };
}

/* global Utility, Elem, extend, Power2 */
'use strict';

function HotSpots(element, data, timeline, eventManager) {
  // Check the first parameter - element
  if (!_.isElement(element)) {
    throw (new Error('You must provide a valid HTMLElement'));
  }
  // Check the second parameter - hotspots array
  if (!_.isPlainObject(data)) {
    throw (new Error('You must provide a HotSpots config object'));
  }
  // Check the fourth parameter - timeline
  if (!_.isObject(timeline) || !_.isFunction(timeline.get)) {
    throw (new Error('You must provide a Timeline instance'));
  }
  // Check the fifth parameter - event manager
  if (!_.isObject(eventManager) || !_.isFunction(eventManager.trigger)) {
    throw (new Error('You must provide an Event Manager instance'));
  }
  // Add the template interface
  Templater.call(this, data);
  // Add the element interface
  ElementInterface.call(this, data);
  // Store the timeline
  this.timeline = timeline;
  // Store the event manager
  this.observer = eventManager;
  // Add this component to the event manager event response
  // @TODO hotspots are not adding to the event object
  // this.observer.addComponent(this);

  // Store the parent container
  this._parent = element;
  // Store the active class for Hotspots
  this._activeClass = 'ivp-hotspot-enabled';
  // Create some hotspot markup, and add them to the timeline
  this.init(data);
}

HotSpots.prototype = {
  /**
   * Broadcast hotspot.show and hotspot.hide events
   * @param {Event type} name
   * @param {DOM Element} element
   */
  _onimpression: function(name, element) {
    // form the event response
    var resp = extend(this.observer.update(), {
      type: name,
      config: this.getItemById(element.dataset.id),
      srcEvent: this
    });
    // Trigger hotspotshow or hotspothide events
    this.observer.trigger(name, resp);
  },
  /**
   * Broadcast hotspot.click and hotspot.remove events
   * @param {Event type} name
   * @param {DOM Event} event
   */
  _onclick: function(name, event) {
    // Do not bubble the click event
    event.stopPropagation();
    // Find the parent element
    var el = Utility.findParent('ivp-hotspot', event);
    // get event data
    var data = this.observer.update();
    // form the event response
    var resp = extend(data, {
      type: name,
      config: this.getItemById(el.dataset.id),
      wasplaying: data.playing,
      srcEvent: event
    });
    // store the wasplaying in the event cache
    this.observer.set({wasplaying: data.playing});
    // trigger hotspotclick
    this.observer.trigger(name, resp);
  },
  /**
   * append Hotspot to correct container
   * @param {Element} Hotspot element
   * @param {data} Hotspot config
   */
  _append: function(element, data) {
    // If no type is set - set it to firefly
    var type = data.type || 'firefly';
    // If the type does not exist - make it
    // also needs to check if adding an element is possible
    if (!this.isType(type) || !_.isElement(this.getType(type))) {
      this.addType(type);
    }
    // Append the element like usual
    this.getType(type).appendChild(element);
  },
  addType: function(name) {
    this._types = this._types || {};
    this._types[name] = Elem.add('div', 'ivp-hotspot-container ivp-hotspot-container--'+name);
    this._parent.appendChild(this._types[name]);
  },
  getType: function(name) {
    return this.isType(name) ? this._types[name] : false;
  },
  isType: function(name) {
    // Check if types exist, and name is inside it
    if (this._types && _.isPlainObject(this._types) && this._types.hasOwnProperty(name)) {
      return true;
    } else {
      return false;
    }
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  eventFilter: function(event) {
    return event;
  },
  /**
   * Add a Hotspot to the timeline
   * @param {HTMLElement} element
   * @param {Integer} timeIn
   * @param {Integer} timeOut
   * @param {Object} position - 'from' config for TimelineLite.fromTo
   * @param {Array} animation - 'to' configs for in and out tweens
   */
  addToTimeline: function(element, timeIn, timeOut, position, animation) {
    // Check that parameters exist
    if (!element || !_.isElement(element) || !_.isNumber(timeIn) || !_.isNumber(timeOut) || !position || !position.unit || !_.isArray(position.coords)) {
      // ignore if incorrect parameters
      return;
    }

    // animation is not required
    if (!animation || !animation.duration) {
      animation = {
        config: [],
        duration: {}
      };
    }

    // Create from obj if it wasn't provided
    if (!animation.hasOwnProperty('from')) {
      animation.from = {};
    }

    // Checking position
    // it must have .unit and .coords array
    var unit = position.unit;
    var coordinate = position.coords[0];
    // Checking animation
    // it must have duration.start duration.end
    var startDuration = animation.duration.start || 0.01;
    var endDuration = animation.duration.end || 0.01;
    // Checking for animation arguements these are not required
    // something doesn't have to be tweened in - could just show up
    var startTweenConfig = (animation.config[0]) ? animation.config[0] : {};
    var endTweenConfig = (animation.config[1]) ? animation.config[1] : {};

    var config = {};
    config.from = extend(animation.from, {
      top: coordinate.y + unit,
      left: coordinate.x + unit
    });

    // starting tween config
    config.start = Utility.merge(startTweenConfig, {
      visibility: 'visible',
      ease: Power2.easeOut,
      onComplete: this._onimpression,
      onCompleteParams: ['hotspot.show', element],
      onCompleteScope: this
    });
    // ending tween config
    config.end = Utility.merge(endTweenConfig, {
      clearProps: 'visibility',
      onStart: this._onimpression,
      onStartParams: ['hotspot.hide', element],
      onStartScope: this
    });
    // Tween hotspot in view
    var startTween = TweenLite.fromTo(element, startDuration, config.from, config.start);
    // Tween hotspot out of view
    var endTween = TweenLite.to(element, endDuration, config.end);
    // Get the TimelineLite instance
    var timeline = this.timeline.get();

    // timeline.call(this.enable, [element], this, timeIn);

    // timeline.call(this.disable, [element], this, timeOut + endDuration);

    // Add the start and end tween to the timeline
    timeline.add(startTween, timeIn).add(endTween, timeOut);
  },
  /**
   * Remove a Hotspot to the timeline
   * @param {HTMLElement} element
   */
  removeFromTimeline: function(element) {
    if (!element || !_.isElement(element)) {
      return;
    }
    // Set opacity to 0 and visibility hidden
    TweenLite.set(element, {autoAlpha:0});
    // fetch the timeline
    var tm = this.timeline.get();
    // TimelineLite allows you to remove all tweens with one stroke
    tm.kill(null, element);

    return this;
  },
  /**
   * Create a Hotspot element and attach the default events
   * @param {Object} item
   * @TODO Default template and error template
   */
  create: function(item) {
    // Check the first parameter - element
    if (!item || !item.hasOwnProperty('id')) {
      throw (new Error('Your object must provide a valid id property'));
    }
    // element classes
    var classes = [];
    // Add the base class
    classes.push('ivp-hotspot');
    // Add the hotspot type
    classes.push('ivp-hotspot--' + (item.type || 'firefly'));
    // Add the hotspot actions as classes
    if (_.isArray(item.actions)) {
      // Create a unique array of the action types
      var actions = _.uniq(_.map(item.actions, 'type'));
      // Add actions types as classes
      _.forEach(actions, function(action) {
        classes.push('ivp-action--' + action);
      });
    }
    // Add hotspot id as class
    classes.push('ivp-hotspot-' + item.id);
    // Create the container
    var el = Elem.add('div', classes.join(' '));
    // Set the ID
    el.setAttribute('data-id', item.id);
    // Render the HTML
    el.innerHTML = this.renderTemplate(item);
    // Create close button
    var exit = Elem.button('Close', 'ivp-btn ivp-btn--close');
    el.appendChild(exit);

    var _this = this;
    // Add a event handler to the remove button
    exit.addEventListener('click', function(event) {
      _this._onclick.call(_this, 'hotspot.remove', event);
    }, false);
    // Add event handler
    el.addEventListener('click', function(event) {
      _this._onclick.call(_this, 'hotspot.click', event);
    }, false);

    return el;
  },
  /**
   * Create Hotspots from config, add them to the registry,
   * add them to the timeline, and attach to the Dom
   * @param {HTMLElement} parent
   * @param [Array]{Object} items
   * @param [Array]{Object} templates
   */
  init: function(data) {
    // If we have no data we can't do much
    if (data.length === 0) {
      return;
    }
    // Store templates
    if (data.templates) {
      this._templates = data.templates;
    }
    // Can not create cards without data
    if (!data.items) {
      return;
    }
    // Store the Hotspot data
    this._data = data.items;
    // Create the hotspots and assign to the timeline
    for (var i = data.items.length - 1; i >= 0; i--) {
      var item = data.items[i];
      // Create element
      var hotspot = this.create(item);
      // Add the element to the Hotspot registry
      this.addElement(hotspot);
      // Add element to Timeline
      if (!_.isPlainObject(item.time) || !_.isPlainObject(item.position) ) {
        return;
      }

      var timeIn = (_.isNumber(item.time.start)) ? item.time.start : 0;
      var timeOut = (_.isNumber(item.time.end)) ? item.time.end : 0;
      this.addToTimeline(hotspot, timeIn, timeOut, item.position, item.animate);
      // Add element to respective container based on its type
      this._append(hotspot, item);
    }
  }
};

// Add the templater interface
Utility.mixin(HotSpots.prototype, Templater.prototype);
// Add the element interface
Utility.mixin(HotSpots.prototype, ElementInterface.prototype);

/* global Utility, Elem, extend */
'use strict';

// Utility for formating seconds into pretty time formats
// @OUTPUT as follows 0:01, 1:01, 10:01, 1:10:01
function formatTime(seconds) {
  // video.duration returns NaN before loadedmetadata fires
  var seconds = _.isNaN(seconds) ? 0 : seconds.toFixed();
  var h = parseInt(seconds / 3600) % 24;
  var m = parseInt(seconds / 60) % 60;
  var s = seconds % 60;
  if (h < 1) {
    // under an hour
    return m + ':' + (s < 10 ? '0' + s : s);
  }
  // over an hour
  return h + ':' + (m < 10 ? '0' + m : m) + ':' + (s  < 10 ? '0' + s : s);
}

// pass parent element and config options
function _createControlElements(el, args) {
  args = args || {};
  // @TODO this should not happen here
  // I am pretty sure we do not even need this merge

  // merge user args with defaults
  var config = Utility.merge(args, {
    play: true,
    stop: false,
    mute: true,
    replay: true,
    scrub: true,
    time: true
  });

  // create tool instance
  var create = new Tool(el);
  // Holder for all elements
  var els = {};
  // Holder for buttons
  els.btn = {};
  // Create the scrubber and attach
  els.scrub = (config.scrub) ? create.scrub() : false;
  // Create the play button and attach
  els.btn.play = (config.play) ? create.play() : false;
  // Create the time elements and attach
  els.time = (config.time) ? create.time(true) : create.time(false);
  // Create the stop button and attach
  els.btn.stop = (config.stop) ? create.stop() : false;
  // Create the mute button and attach
  els.btn.mute = (config.mute) ? create.mute() : false;
  // Create the replay button and attach
  els.btn.replay = (config.replay) ? create.replay() : false;

  return els;
}

function Tool(el) {
  var parent = el;

  // Use this to attach children
  function _attach(child) {
    parent.appendChild(child);
    return child;
  }
  // Create markup for time nodes
  function _timeChildren(el, text) {
    // Create the markup
    var wrap = document.createElement('div');
    var controlText = Elem.add('span', 'ivp-control-text');
    var controlValue = Elem.add('span', 'ivp-control-value');
    // Set some default values inserted before playerReady
    controlText.innerHTML = text;
    controlValue.innerHTML = '0:00';
    // Attach elements
    wrap.appendChild(controlText);
    wrap.appendChild(controlValue);
    el.appendChild(wrap);
  }

  // Creates play button
  this.play = function() {
    var el = Elem.button('Play', 'ivp-btn', 'ivp-btn--play');
    el.setAttribute('aria-live', 'polite');
    el.setAttribute('tabindex', '0');

    _attach(el);
    return el;
  };
  // Create stop button
  this.stop = function() {
    var el = Elem.button('Stop', 'ivp-btn', 'ivp-btn--stop');
    el.setAttribute('aria-live', 'polite');
    el.setAttribute('tabindex', '0');
    _attach(el);
    return el;
  };
  // Create mute button
  this.mute = function() {
    var el = Elem.button('Mute', 'ivp-btn', 'ivp-btn--mute');
    el.setAttribute('aria-live', 'polite');
    el.setAttribute('tabindex', '0');
    _attach(el);
    return el;
  };
  // Create replay
  this.replay = function() {
    var el = Elem.button('Replay', 'ivp-btn', 'ivp-btn--replay');
    el.setAttribute('aria-live', 'polite');
    el.setAttribute('tabindex', '0');
    _attach(el);
    return el;
  };
  // Create time elements - if not visible add a special class
  this.time = function(visible) {

    var custom = (visible) ? '' : 'ivp-hide';

    // Current Time
    var current = Elem.add('div', 'ivp-current-time ivp-time-controls ivp-control', custom);
    _timeChildren(current, 'Current Time ');
    // Divider
    var divider = Elem.add('div', 'ivp-time-divider', custom);
    var dividerSpan = Elem.add('span');
    dividerSpan.innerHTML = '/';
    divider.appendChild(dividerSpan);
    // Duration Time
    var duration = Elem.add('div', 'ivp-duration ivp-time-controls ivp-control', custom);
    _timeChildren(duration, 'Duration Time ');
    // Remaining Time
    var remain = Elem.add('div', 'ivp-remaining-time ivp-time-controls ivp-control', custom);
    _timeChildren(remain, 'Remaining Time ');

    return {
      current: _attach(current),
      divider: _attach(divider),
      duration: _attach(duration),
      remain: _attach(remain)
    };
  };
  // Create scrubber
  this.scrub = function() {
    var container = Elem.add('div', 'ivp-control-progress', '');
    var progress = Elem.add('div', 'ivp-progress-bar', '');
    var time = Elem.add('span', 'ivp-progress-time', '');
    progress.appendChild(time);
    container.appendChild(progress);

    // A lot more aria roles are coming to this spot!

    return {
      scrub: _attach(container),
      progress: progress,
      time: time
    };
  };

}

/* global formatTime */
'use strict';

// utility for dispatching seeked, seeking events as well as moving a scrub bar
function Scrub(el, player, events) {

  var _this = this;

  var playing;

  var lastTouchedX = 0;

  // Calculates position percentage
  _this.position = function(el, event) {
    var x;
    if (!event.touches) {
      // mouse events
      x = event.clientX;
    } else if (event.touches.length > 0) {
      // touchstart/touchmove events
      x = event.touches[0].clientX;
      lastTouchedX = x;
    } else {
      // touchend does not have a clientX
      x = lastTouchedX;
    }

    var rect = el.getBoundingClientRect();

    var position = x - rect.left;
    var percentage = 100 * position / el.offsetWidth;

    // This can be done cleaner
    if (percentage > 100) {
      percentage = 100;
    } else if (percentage < 0) {
      percentage = 0;
    }
    return percentage;
  };

  _this.to = function(percent) {
    // This is for sanity but also for visual. The playhead shouldn't leave the progress bar.
    var clamp = Math.min(Math.max(percent, 0.2), 100);
    // Convert this into matrices stuff later
    var style = 'translateX(' + clamp + '%)';
    el.time.style.webkitTransform = style;
    el.time.style.MozTransform = style;
    el.time.style.msTransform = style;
    el.time.style.OTransform = style;
    el.time.style.transform = style;
  };

  _this.on = function(event) {
    event.stopPropagation();
    // stop listening for seek events - we are causing them
    events.off('seeked', _this.manualSeek);
    // Set the play state for when we let go
    playing = player.isPlaying();
    // Stop the playhead from moving
    if (playing) {
      window.cancelAnimationFrame(myReq);
    }
    // Pause the video
    player.pause();
    // Fire the move event once
    _this.move(event);
    // Start listening for mouse events
    document.addEventListener('mousemove', _this.move, false);
    document.addEventListener('mouseup', _this.off, false);
    // And touch events
    document.addEventListener('touchmove', _this.move, false);
    document.addEventListener('touchend', _this.off, false);
  };

  _this.off = function(event) {
    // Calculate play position
    var percent = _this.position(el.progress, event);
    var time = player.duration() * percent / 100;
    // Set new play position
    player.currentTime(time);
    // If we were playing lets play again
    if (playing) {
      player.play();
    }
    // Re-enable the seeked listener
    events.on('seeked', _this.manualSeek);
    // Remove listeners for the mouse
    document.removeEventListener('mousemove', _this.move, false);
    document.removeEventListener('mouseup', _this.off, false);
    // and touch events
    document.removeEventListener('touchmove', _this.move, false);
    document.removeEventListener('touchend', _this.off, false);
  };

  _this.move = function(event) {
    // Calculate play position
    var percent = _this.position(el.progress, event);
    var time = player.duration() * percent / 100;
    // Move the playhead
    _this.to(percent);
    // Scrub the video
    player.currentTime(time);
  };

  _this.onTimeUpdate = function() {
    var percent = 100 * player.currentTime() / player.duration();
    _this.to(percent);
    myReq = requestAnimationFrame(_this.onTimeUpdate);
  };

  _this.manualSeek = function(event) {
    var percent = 100 * event.time / player.duration();
    _this.to(percent);
  }

  // Listen for manual seek events - i.e timeUpdate
  events.on('seeked', _this.manualSeek);
  // Adjust seek bar if user clicks it
  el.progress.addEventListener('mousedown', _this.on, false);
  el.progress.addEventListener('touchstart', _this.on, false);

  // desperate need for refactor
  var myReq;

  events.on('loadedmetadata', function(e) {
    _this.to(0);
  });

  events.on('play', function(e) {
    myReq = requestAnimationFrame(_this.onTimeUpdate)
  });

  events.on('pause', function(e){
    window.cancelAnimationFrame(myReq);
  });

}

/* global Utility, Elem, extend, formatTime */
'use strict';

function Controls(controlElement, widgetNode, settings, player, events) {

  var _this = this;

  // Generate all the tools
  var tool = _createControlElements(controlElement, settings);

  // Save these for sanity
  var currentEl = tool.time.current.querySelector('.ivp-control-value');
  var remainEl = tool.time.remain.querySelector('.ivp-control-value');

  _this._timer = false;

  // Hey we just seeked, what do i do now?
  // @TODO this should happen on either Player or Video not on controls
  // _this.onSeeked = function(event) {
  //   // Play the video if we were playing before
  //   if (event.wasplaying) {
  //     player.play();
  //   }
  //   // tool.btn.play.classList.remove('scrubing');
  //   if (tool.hasOwnProperty('scrub')) {
  //     tool.scrub.scrub.classList.remove('scrubbing')
  //   }
  //   // make sure the correct time is set
  //   _this.onSeeking(event);
  // };

  // _this.onSeekStart = function(event) {
  //   if (event.wasplaying) {
  //     tool.btn.play.classList.add('ivp-btn--playscrubing');
  //   }
  //   if (tool.hasOwnProperty('scrub')) {
  //     tool.scrub.scrub.classList.add('scrubbing')
  //   }
  //   _this.onSeeking(event);
  // };

  _this.onSeeking = function(event) {
    // Update the current playhead position time
    var current = Math.round(event.time);
    currentEl.innerHTML = formatTime(current);
  };

  _this.onLoadedMeta = function() {
    _this.duration = player.duration();
    tool.time.duration.querySelector('.ivp-control-value').innerHTML = formatTime(_this.duration);
  };

  _this.onPlayToggle = function(event) {
    event.stopPropagation();
    player.playToggle();
  };

  _this.onPlay = function() {
    tool.btn.play.classList.add('ivp-btn--playing');
    var span = tool.btn.play.querySelector('span');
    span.innerHTML = 'Pause';
  };

  _this.onPause = function() {
    if (tool.btn.play) {
      tool.btn.play.classList.remove('ivp-btn--playing');
      var span = tool.btn.play.querySelector('span');
      span.innerHTML = 'Play';
    }
  };

  _this.onMute = function(event) {
    var textSpan = tool.btn.mute.querySelector('span');
    // If the video is muted
    if (event.srcEvent.target.muted) {
      textSpan.innerHTML = 'UnMute';
      tool.btn.mute.classList.add('ivp-btn--mute-muted');
    } else {
      textSpan.innerHTML = 'Mute';
      tool.btn.mute.classList.remove('ivp-btn--mute-muted');
    }
  };

  _this.onSlowTimeUpdate = function(event) {
    var current = Math.round(event.time);
    currentEl.innerHTML = formatTime(current);
    remainEl.innerHTML = '-' + formatTime(_this.duration - current);
  };
  // Click handler for Mute toggle button
  _this.muteClick = function(event) {
    event.stopPropagation();
    // get the current mute state
    var state = player.mute();
    // set the new mute state
    player.mute(!state);
  };

  _this.setActive = function() {
    widgetNode.classList.remove('ivp-player-inactive');
    widgetNode.classList.add('ivp-player-active');
  };

  _this.setInactive = function() {
    widgetNode.classList.remove('ivp-player-active');
    widgetNode.classList.add('ivp-player-inactive');
  };

  // Set the current time to 0
  // Move the scrub bar to original position

  var mouseEnter = function() {
    clearTimeout(_this._timer);
    _this.setActive();
    widgetNode.removeEventListener('mouseenter', mouseEnter, false);
  };

  var mouseLeave = function() {
    if (!player.isPlaying()) {
      return;
    }
    _this._timer = Utility.setTimeoutContext(function() {
      _this.setInactive();
    }, 500, _this);
    widgetNode.addEventListener('mouseenter', mouseEnter, false);
  };

  // Mouse Enter
  widgetNode.addEventListener('mouseenter', mouseEnter, false);
  // Mouse Leave
  widgetNode.addEventListener('mouseleave', mouseLeave, false);

  // Duration isn't availible until the video plays the first time
  events.on('loadedmetadata', _this.onLoadedMeta);
  // Update the current time and remaining time
  // events.on('timeupdate', Utility.throttle(_this.onSlowTimeUpdate, 800));
  events.on('timeupdate', _.throttle(_this.onSlowTimeUpdate, 500));

  // Video ended bring the current time to 0
  events.on('ended', function() {
    currentEl.innerHTML = formatTime(player.duration());
    _this.setActive();
  });

  // Scrub Bar
  if (tool.scrub) {
    new Scrub(tool.scrub, player, events);
    // events.on('seekstart', _this.onSeekStart);
    // events.on('seeked', _this.onSeeked);
    events.on('seeking', _this.onSeeking);
  }
  // Play Button
  if (tool.btn.play) {
    events.on('play', _this.onPlay);
    events.on('pause', _this.onPause);
    tool.btn.play.addEventListener('click', _this.onPlayToggle, false);
  }
  // Stop Button
  if (tool.btn.stop) {
    tool.btn.stop.addEventListener('click', player.stop.bind(player), false);
  }
  // Replay Button
  if (tool.btn.replay) {
    tool.btn.replay.addEventListener('click', player.replay.bind(player), false);
  }
  // Mute toggle
  if (tool.btn.mute) {
    events.on('volumechange', _this.onMute);
    tool.btn.mute.addEventListener('click', _this.muteClick, false);
  }

}

Controls.prototype = {};

/* global Utility */
/* exported defaultBehavior */
'use strict';

var defaultManagerListeners = function(scope) {

  return {
    // When a user clicks a hotspot
    onhotspotclick: {
      name: 'hotspot.click',
      func: function(event) {
        // Store action ID so duplicate actions are not performed
        var ids = [];
        // Filter the event actions only for Cards
        var actions = _.filter(event.config.actions, {type: 'card'});
        // If no card actions exist get out of here
        if (actions.length < 0) {
          return;
        }
        // a card action pauses the video by default
        scope.player.pause();
        // Execute each Card action
        _.forEach(actions, function(action) {
          // Do not execute the same action twice
          if (ids.indexOf(action.target) > -1) {
            return;
          }
          // Enable the card by passing the cardID
          scope.card.enable(action.target);
          // Add this id to the ran ids list
          ids.push(action.id);
        });
      }
    },
    // When a user hides a hotspot
    onhotspotremove: {
      name: 'hotspot.remove',
      func: function(event) {
        var el = Utility.findParent('ivp-hotspot', event.srcEvent);
        // remove the hotspot from the timeline - never to show again
        scope.hotspot.removeFromTimeline(el);
      }
    },
    // When a user hides a card
    oncardhide: {
      name: 'card.hide',
      func: function(event) {
        if (event.wasplaying) {
          scope.player.play();
        }
        // remove the wasplaying value from the event cache
        scope.observer.clear('wasplaying');
      }
    },
    // When a video plays for the first time
    onfirstplay: {
      name: 'firstplay',
      func: function() {
        scope.poster.disable();
      }
    },
    onended: {
      name: 'ended',
      func: function() {
        if (scope.hasOwnProperty('endscreen') && _.isFunction(scope.endscreen.enable)) {
          scope.endscreen.enable();
        }
      }
    }
  };

};


var defaultDomListeners = function(elements, scope) {

  return {
    // Clicking the playback container will toggle video playback
    onplaybackclick: {
      name: 'playback.click',
      event: ['click'],
      node: elements.playback,
      func: function(event) {
        event.stopPropagation();
        scope.player.playToggle();
      }
    },
    // Clicking the card parent with close all active cards
    oncardclick: {
      name: 'cards.click',
      event: ['click'],
      node: elements.cards,
      func: function(event) {
        event.stopPropagation();
        scope.card.disable();
      }
    },
    // Clicking a poster will play a video
    onposterclick: {
      name: 'poster.click',
      event: ['click'],
      node: elements.poster,
      func: function(event) {
        event.stopPropagation();
        scope.player.play();
      }
    },
    // Clicking an endescreen will replay a video
    onendscreenclick: {
      name: 'endscreen.click',
      event: ['click'],
      node: elements.endscreen,
      func: function(event) {
        event.stopPropagation();
        if (scope.hasOwnProperty('endscreen') && _.isFunction(scope.endscreen.disable)) {
          scope.endscreen.disable();
          scope.player.play();
        }
      }
    }
  };
};


function defaultBehavior(elements, scope) {
  // store oberserver for binding
  var observer = scope.observer;
  // get all default manager event listeners
  var managerApi = defaultManagerListeners(scope);
  // get all default dom event listeners
  var domApi = defaultDomListeners(elements, scope);
  // search methods for registered event listeners
  var lookup = {
    manager: function(name) {
      return _.find(managerApi, {name: name});
    },
    dom: function(name) {
      return _.find(domApi, {name: name});
    }
  };

  return {
    // enable all default behaviors
    enable: function() {
      // enable all the manager events
      _.forEach(managerApi, function(value) {
        observer.on(value.name, value.func);
      });
      // enable all the dom events
      _.forEach(domApi, function(value) {
        // Each event type - click, touchstart, etc
        _.forEach(value.event, function(e) {
          value.node.addEventListener(e, value.func, false);
        });
      });
    },
    // disable all default behaviors
    disable: function() {
      _.forEach(managerApi, function(method) {
        observer.off(method.name, method.func);
      });
      // disable all the dom events
      _.forEach(domApi, function(value) {
        // Each event type - click, touchstart, etc
        _.forEach(value.event, function(e) {
          value.node.removeEventListener(e, value.func, false);
        });
      });
    },
    // turn on a single default behavior
    on: function(name) {
      // Check if event name is manager behavior
      var value = lookup.manager(name);
      if (_.isPlainObject(value)) {
        observer.on(value.name, value.func);
        return;
      }
      // Check if event name is a dom behavior
      value = lookup.dom(name);
      if (_.isPlainObject(value)) {
        _.forEach(value.event, function(e) {
          value.node.addEventListener(e, value.func, false);
        });
      }
    },
    // turn off a single default behavior
    off: function(name) {
      // Check if event name is manager behavior
      var value = lookup.manager(name);
      if (_.isPlainObject(value)) {
        observer.off(value.name, value.func);
        return;
      }
      // Check if event name is a dom behavior
      value = lookup.dom(name);
      if (_.isPlainObject(value)) {
        _.forEach(value.event, function(e) {
          value.node.removeEventListener(e, value.func, false);
        });
      }
    }
  };

}

/* global Utility, Elem, extend */
'use strict';

function Cards(element, data, observer) {
  // Check the first parameter - element
  if (!_.isElement(element)) {
    throw (new Error('You must provide a valid HTMLElement'));
  }
  // Check the second parameter - data object
  if (!_.isPlainObject(data)) {
    throw (new Error('You must provide a valid data object'));
  }
  // Check the third parameter - observer or event manager
  if (!_.isObject(observer) || !_.isFunction(observer.trigger)) {
    throw (new Error('You must provide an event manager'));
  }
  // Inherit the template interface
  Templater.call(this, data);
  // Inherit the element interface
  ElementInterface.call(this, data);
  // Store the event manager
  this.observer = observer;
  // Add this component to the event manager event response
  // @TODO currently cards does not contribute to the event object
  // this.observer.addComponent(this);

  // Lets create some Cards!
  this.init(element, data);
}

Cards.prototype = {
  /**
   * Get Card data by Id
   * @param {Integer || String} id
   */
  getCardById: function(id) {
    // Make sure we are passing something safe into parseInt
    if (_.isNull(id) || _.isPlainObject(id) || _.isFunction(id)) {
      return;
    }
    // Should check if this._templates exists if not create it and return false
    var index = _.findIndex(this._items, {id: parseInt(id)});
    // return the template or undefined
    return (index > -1) ? this._items[index] : false;
  },
  /**
   * Event Filter
   * @param {Obj} event
   * @returns event response
   */
  eventFilter: function(event) {
    return event;
  },
  /**
   * Get the active Card
   * returns {Object} id, element, data
   * @TODO Implement this feature
   */
  getActive: function() {},
  /**
   * Create elements from a Card template and register default events
   * @param {Object} card
   */
  create: function(card) {
    var el = Elem.add('div', 'ivp-card');
    // Render the template and add the content
    el.innerHTML = this.renderTemplate(card);
    // Add the Card ID
    el.setAttribute('data-id', card.id);
    // Create close button
    var exit = Elem.button('Close', 'ivp-btn ivp-btn--close');
    el.appendChild(exit);

    // store scope ref for addEventListener
    var _this = this;
    // Add a event handler to the Close button
    exit.addEventListener('click', function(event) {
      event.stopPropagation();
      _this.disable();
      // _this.close.call(_this, event);
    }, false);

    // This is a work around for IE lack of pointer-events support
    el.addEventListener('click', function(event) {
      event.stopPropagation();
    });

    return el;
  },
  /**
   * @TODO THIS METHOD DOES NOT LINT
   * Visually show an element
   * @param {HTMLElement} element
   * @param {Integer} id
   */
  // Visually show an element
  show: function(element, data) {
    // We can not show an element without its data
    if (!element || !_.isPlainObject(data)) {
      return;
    }

    // Enable the Card container
    this.parent.classList.add('ivp-cards-enabled');
    // form the event response
    var resp = extend(this.observer.update(), {
      type: 'card.show',
      config: data,
      srcEvent: this
    });
    // We can not make a tween without the correct args
    // add the active class the element and trigger the event
    if (!data.animate || !_.isArray(data.animate.config) || !data.animate.duration.start) {
      element.classList.add('ivp-card-enabled');
      this.observer.trigger('card.show', resp);
      return element;
    }

    // @TODO we could do some TweenLite.set stuff with this data before the tween
    // Positioning
    // data.position.unit
    // data.position.x
    // data.position.y

    // Tween the card into view
    //var tween = TweenLite.to(element, data.animate.duration.start, data.animate.config[0]);

    element.classList.add('ivp-card-enabled');
    // nice callback bro
    var callBack = function() {
      this.observer.trigger('card.show', resp);
    };

    var duration = data.animate.duration.start;
    // Create the tween From args
    var configFrom = (data.animate.from) ? data.animate.from : {};
    var fromArgs = Utility.merge(configFrom, {
      x: data.position.x,
      y: data.position.y
    });

    // Create the tween To args
    var toArgs = Utility.merge(data.animate.config[0], {
      onComplete: callBack,
      onCompleteScope: this
    });
    // Create the Tween instance
    // var tween = TweenLite.fromTo(element, duration, fromArgs, toArgs);
    TweenLite.fromTo(element, duration, fromArgs, toArgs);
    // if(tween) {
    //   tween.eventCallback('onComplete', function() {
    //     element.classList.add('ivp-card-enabled');
    //     // @TODO Figure out the event object to return
    //     this.observer.trigger('cardshow', {eventName: 'cardshow', element: element, data: data});
    //   }, false, this);
    // }

    return element;
  },
  /**
   * Visually hide an element
   * @param {HTMLElement} element
   * @param {Object} data
   */
  hide: function(element, data) {
    // We can not hide an element without its data
    if (!element || !_.isPlainObject(data)) {
      return true;
    }
    // form the event response
    var resp = extend(this.observer.update(), {
      type: 'card.hide',
      config: data,
      srcEvent: this
    });
    // We can not make a tween without the correct args
    // add the active class the element and trigger the event
    if (!data.animate || !_.isArray(data.animate.config) || !data.animate.duration.end) {
      element.classList.remove('ivp-card-enabled');
      this.parent.classList.remove('ivp-cards-enabled');
      this.observer.trigger('card.hide', resp);
      return element;
    }
    // The animation args could be an array of 1 item. Check the length before assigning
    var animation = (data.animate.config.length > 1) ? data.animate.config[1] : data.animate.config[0];
    // Create the tween
    var tween = TweenLite.to(element, data.animate.duration.end, animation);


    this.parent.classList.remove('ivp-cards-enabled');
    element.classList.add('ivp-card-removing');
    // Create callback for when tween finishes
    // Remove the active class and trigger the cardhide event
    if (tween) {
      tween.eventCallback('onComplete', function() {
        element.classList.remove('ivp-card-enabled');
        element.classList.remove('ivp-card-removing');
        // @TODO Figure out the event object to return
        this.observer.trigger('card.hide', resp);
      }, false, this);
    }

    return element;
  },
  /**
   * @TODO THIS METHOD DOES NOT WORK
   * Enable a Card - get Card by ID, set it active, and show it
   * @param {Integer} id
   */
  enable: function(id) {
    if (_.isUndefined(id)) {
      return;
    }
    // Get the card by ID
    var card = this.getElement(id);
    // Get the Card data
    var data = this.getItemById(id);
    // Store the active card
    // @TODO Implement the active card feature
    // @TODO The active card feature needs getActive
    this.active = {id: id, el: card, data: data};
    // Pass the card element and its data
    this.show(card, data);
  },
  /**
   * Disable the active Card and hide it
   */
  disable: function() {
    // Check if anything is inside the active storage
    if (!_.isPlainObject(this.active) || Object.keys(this.active).length === 0) {
      return false;
    } else if (!this.active.el || !this.active.data) {
      // this.active had length but did not store in the correct format
      // We will not be hiding a card because we don't know which to hide
      this.active = {};
      return this.active;
    }

    // Hide the card
    this.hide(this.active.el, this.active.data);

    // Clear active cards
    // @TODO Clear active should be a method
    this.active = {};
    return;
  },
  /**
   * Let it Roll - Create each Card
   * @param {Object} settings
   */
  init: function(element, data) {
    // Create holder for active card
    this.active = {};
    // Create Card container
    this.parent = element;
    // If we have no data we can't do much
    // Can not create cards without data
    if (data.length === 0 || !data.items) {
      return;
    }
    // Use the Card data to generate Cards
    for (var i = data.items.length - 1; i >= 0; i--) {
      // Create the markup
      var el = this.create(data.items[i]);
      // Add the card to the registry
      this.addElement(el);
      // Add the card to the parent node
      this.parent.appendChild(el);
    }

  }

};

// Add the interfaces
Utility.mixin(Cards.prototype, Templater.prototype);

Utility.mixin(Cards.prototype, ElementInterface.prototype);

/* global Utility */
'use strict';

function EventManager(widget, scope) {

  this._el = widget;

  this._scope = scope || this;

  this._components = [];

  this._response = {
    // used by ctaView log event
    pageName: null,
    // used throughout Invodo log events
    pod: null,
    // used when manually pausing a video, like clicking a hotspot
    wasplaying: null,
    // sometimes dom events are stored here and sometimes inplayer original events
    srcEvent: null,
    // when interactiong with hotspots or cards there config will be filled here
    config: null
  };

}

EventManager.prototype = {

  on: function(event, fct) {
    this._events = this._events || {};
    this._events[event] = this._events[event] || [];
    this._events[event].push(fct);
  },

  off: function(event, fct) {
    this._events = this._events || {};
    if (event in this._events === false) { return; }
    this._events[event].splice(this._events[event].indexOf(fct), 1);
  },

  // Modified EventEmitter trigger - returns Manager scope
  trigger: function(event /* , args... */) {
    this._events = this._events || {};

    if (event in this._events) {
      for (var i = 0; i < this._events[event].length; i++) {
        this._events[event][i].apply(this._scope, Array.prototype.slice.call(arguments, 1));
      }
    }
    // dispatch external event
    this.dispatch(event, Array.prototype.slice.call(arguments, 1));
  },

  dispatch: function(event, data) {
    var dispatch = [];
    var custom;
    // Check for dot convention in event name
    if (event.indexOf('.') !== -1) {
      // Add the text before the dot to the dispatch que
      var root = event.split('.')[0];
      // Add the events to the dispatch que
      dispatch.push(root, event);
      // Dispatch each event
      for (var i = 0; i < dispatch.length; i++) {
        custom = new CustomEvent(dispatch[i], {detail: data[0], bubbles: false, cancelable: true});
        this._el.dispatchEvent(custom);
      }
    } else {
      custom = new CustomEvent(event, {detail: data[0], bubbles: false, cancelable: true});
      this._el.dispatchEvent(custom);
    }
  },

  // add a component to components list
  // components are tapped for event data
  addComponent: function(comp) {
    this._components.push(comp);
  },

  // update the event response cache by requesting data from each subcomponent
  update: function() {
    // create container by cloning the cache event response
    var res = _.clone(this.get(), true);
    // poll each registered component for new data
    for (var i = 0; i < this._components.length; i++) {
      this._components[i].eventFilter(res);
    }
    // return event data
    return res;
  },

  // Get the cached event response
  get: function() {
    return this._response;
  },
  set: function(data) {
    this._response = extend(this._response, data);
    return this._response;
  },
  // clear a key in the cache - not the entire cache
  clear: function(key) {
    // check to make sure key exists in cache
    if (!this._response.hasOwnProperty(key)) {
      return;
    }
    // Clear the event response key
    this._response[key] = null;
  },
  // destroy the event response cache
  destroy: function() {
    this._response = {};
  }
};

/* global Ivp, Utility, Elem, extend, defaultBehavior, Screen, NativeVideo, addEventListeners, removeEventListeners */
'use strict';

function errorScreen(el) {
  var error = Elem.add('div', 'ivp-error');
  var text = Elem.add('p');
  text.innerHTML = 'The Interactive Video Player could not be loaded. </br> The Reference Id is invalid';
  error.appendChild(text);
  el.appendChild(error);
}

/**
 * Format user provide arguements with Ivp defaults
 * @returns {Obj} settings
 */
function managerConfiguration(args) {
  // Clone the user provided args
  var config = _.clone(args);
  // config gets the Ivp.defaults and a uniqueId
  return _.defaults(config, Ivp.defaults, {domId: Utility.uniqueId()});
}

/**
 * Retrieve Ivp data from the Invodo.Pod with a Promise
 * @returns {Obj} settings
 */
function fetchPod(refId) {
  // Return the promise for chaining
  return new Promise(function(resolve, reject) {
    // successfully resolve the promise
    var onload = function(pod) {
      window.clearTimeout(timeout);
      resolve(pod);
    };
    // could not get data from pod - refId could be invalid
    var onfail = function() {
      reject('The Ivp Reference Id appears to be invalid.');
    };
    // Invodo doesn't let us know if the pod is invalid
    // Create a 4 second timeout and clear it when the pod responds
    var timeout = window.setTimeout(onfail, 4000);
    // Check if the refID was previously loaded outside of the experience
    var pod = Invodo.Pod.getByRefId(refId);
    // resolve the promise if we have pod data
    if(!_.isNull(pod)) {
      onload(pod);
    } else {
      Invodo.Pod.load({mpd: refId}, onload);
    }
  });
}

/**
 * Create DOM elements for the Manager
 * @returns {Obj} of Elements
 */
function managerElements(id, name, theme, normalize) {
  // Add a reliable, unmuteable name to the widget
  var classes = ['ivp-widget'];
  // Add the normalize class to the widget
  if (normalize) {
    classes.push('ivp-normie');
  }
  // Add the client namespace, theme name to the widget
  // Add the active class to the widget so the controls appear fullsize
  classes.push(name, theme, 'ivp-player-off');
  // Create all the dom elements
  var element = {
    widget: Elem.container(id, name, classes.join(' ')),
    poster: Elem.add('div', 'ivp-poster'),
    endscreen: Elem.add('div', 'ivp-endscreen ivp-hide'),
    playback: Elem.add('div', 'ivp-playback'),
    cards: Elem.add('div', 'ivp-cards'),
    hotspot: Elem.add('div', 'ivp-hotspots'),
    video: Elem.add('div', 'ivp-video'),
    controls: Elem.add('div', 'ivp-controls')
  };

  // Attach elements to the playback container
  _([element.video, element.hotspot, element.cards]).forEach(function(el) {
    element.playback.appendChild(el);
  }).value();

  // Attach elements to widget container
  _([element.poster, element.playback, element.controls, element.endscreen]).forEach(function(el) {
    element.widget.appendChild(el);
  }).value();

  return element;
}

// Utility for handling Invodo Pod data
var podFormat = (function(){

  return {
    // foramt Invodo Pod CustomData fields
    customData: function(data) {
      var custom = Invodo.Pod.getCustomData(data.pod);
      var k, v;
      var obj = {};
      _.forEach(custom, function(d) {
        k = d[0];
        v = d[1];
        obj[k] = v;
      });
      return obj;
    },

    // format Invodo Pod frames for video data
    frames: function(data) {
      var processed = [];
      // should error check in here for config.frames
      _.forEach(data.config.frames, function(frame) {
        var obj = {
          id: frame.durableId,
          durableId: frame.durableId,
          poster: frame.previewUrl.substr(5),
          duration: frame.duration,
          title: frame.title,
          encoding: {
            default: frame.defaultEncoding,
            items : []
          }
        };
        // push each encoding
        _.forEach(frame.encodings, function(encoding){
          obj.encoding.items.push({
            id: encoding.id,
            bitrate: encoding.bitrate,
            src: encoding.http.substr(5),
            type: 'video/mp4',
            text: encoding.label
          });
        });
        processed.push(obj);
      });

      var multiclip = !(processed.length === 1);

      return {
        multiclip: multiclip,
        items: processed
      };
    },

    // format the presentation
    // for some reason you cannot get the title and description directly from the pod response
    presentation: function(data) {
      return {
        title: Invodo.Pod.getTitle(data.pod),
        text: Invodo.Pod.getDescription(data.pod)
      };
    },

    // Format the entire response
    response: function(data) {
      // Create the podConfig
      var config = {
        // Store the page name - used in reporting
        pageName: Invodo.getPageName(),
        // store the pod
  	    pod: data.pod,
  	    // store the refId
  	    refId: data.mpd,
  	    // a experience represents all the video clips - inplayer calls this a presentation
  	    experience: podFormat.presentation(data),
  	    // want to save the config?
  	    config: data.mpdPath,
  	    // process the frames object
  	    frames: podFormat.frames(data),
  	    // process the custom data object
  	    manifest: podFormat.customData(data)
  	  };
      return {
        podConfig: config
      };
    }
  };

})();


function managerSettings(settings) {
  var stored = settings;
  return {
    get: function() {
      return stored;
    },
    set: function(obj) {
      stored = extend(stored, obj);
    }
  };
}

function Manager(node, args, callback) {
  // If node is not an element
  if (!_.isElement(node)) {
    this.onerror('You must provide a valid HTMLElement');
    //throw (Error('You must provide a valid HTMLElement'));
  }
  // If node is not attached to the dom
  else if (!node.ownerDocument.body.contains(node)) {
    this.onerror('The element provided is not attached to the DOM!');
  }
  // Checking for the required Invodo RefId
  else if (!args.hasOwnProperty('refId')) {
    this.onerror('You must provide a valid RefID in your Ivp configuration');
  }

  var _this = this;
  // Create the manager settings
  var settings = managerConfiguration(args);
  // Create the manager nodes
  var elements = managerElements(settings.domId, settings.name, settings.theme, settings.normalize);
  // Store the outermost node for binding events
  this._node = node;
  // Settings manager
  this._settings = managerSettings(settings);
  // Create the event Manager
  this.observer = new EventManager(this._node, this);
  // Assert the default behaviors
  this.defaults = defaultBehavior(elements, this, this.observer);
  // Enable the default event handlers
  this.defaults.enable();
  // Create a poster instance as soon as possible
  this.poster = new Screen(elements.poster, elements.widget, this.observer);




  //
  //
  // Move everything below into a function to get it out of Manager
  //
  //


  // Fetch Json for Pod or external link
  var fetchJson = function(path) {
    // Ivp.fetch returns a Promise
    return Ivp.fetch(path).get().then(function(resp) {
      //settings.data = JSON.parse(resp);
      return {
        data: JSON.parse(resp)
      };
    });
  };

  var allPromises = [];

  var pod = fetchPod(settings.refId).then(podFormat.response);

  var data = new Promise(function(resolve, reject) {
    // If data is availible we are done here
    if (_.isPlainObject(settings.data)) {
      resolve();
    }
    // If a user has provided an external link to data attempt to fetch it
    else if (_.isString(settings.dataPath)) {
      fetchJson(settings.dataPath).then(resolve).catch(reject);
    }
    // With no data provided, we must fetch it from the pod after the pod loads
    else {
      pod.then(function(resp) {
        fetchJson(resp.podConfig.manifest.ivp.data).then(resolve).catch(reject);
      });
    }
  });

  allPromises.push(data, pod);

  Promise.all(allPromises).then(function(objs){
    // make sure we are working with objects
    var cleaned = _.remove(objs, function(n) {
      return _.isPlainObject(n);
    });
    // Add each object to the settings
    _.forEach(cleaned, function(obj) {
      _this._settings.set(obj);
    });
    // Create the components
    _this.init(elements, settings, callback);
  }).catch(function(value) {
    errorScreen(node);
    console.warn(value, 'Reference Id = ' + settings.refId);
  });

}

/**
 * Init the Manager
 *
 */
Manager.prototype.init = function(element, settings, callback) {
  // Store the pod config - returned by Invodo.Pod.load
  this.observer.set({ pageName: settings.podConfig.pageName, pod: settings.podConfig.pod });
  // Give the poster some data
  this.poster.update(settings.data.poster);
  // Create a player
  this.player = new NativeVideo(element.video, settings.podConfig.frames, this.observer, settings.defaultVideo);
  // Create the Timeline
  this.timeline = new Timeline(this.observer);
  // Create a HotSpots instance - pass the parent element, hotspots data, timeline, and the event manager
  this.hotspot = new HotSpots(element.hotspot, settings.data.hotspot, this.timeline, this.observer);
  // Create the controls
  this.control = new Controls(element.controls, element.widget, settings.controls, this.player, this.observer);
  // Create a Cards instance - pass the parent element, cards data, and the event manager
  this.card = new Cards(element.cards, settings.data.card, this.observer);
  // Enable Invodo logging
  this.logging(element);
  // Create an endscreen
  if (settings.data.hasOwnProperty('endscreen')) {
    this.endscreen = new Screen(element.endscreen, element.widget, this.observer, settings.endscreen);
    // Display an endscreen when a video finishes
    // this.defaults.on('endscreen.show');
    // Clicking on an endscreen will replay the video
    // this.defaults.on('endscreen.click');
  }
  // Attach new widget to node and we are ready to run
  this._node.appendChild(element.widget);
  // Fire the callback
  if (_.isFunction(callback)) {
    callback.call(this);
  }
};

/**
 * Get the manager settings
 * @returns {Object}
 */
Manager.prototype.settings = function() {
  return this._settings.get();
};

/**
 * Handle errors
 * @returns Error
 */
Manager.prototype.onerror = function(message){
  throw (Error(message));
};

/**
 * Play the experience
 * @returns Manager
 */
Manager.prototype.play = function() {
  this.player.play();
  return this;
};

/**
 * Pause the experience
 * @returns Manager
 */
Manager.prototype.pause = function() {
  this.player.pause();
  return this;
};

/**
 * Toggle the experience Playback state
 * @returns Manager
 */
Manager.prototype.toggle = function() {
  this.player.playToggle();
  return this;
};

/**
 * Stop the experience
 * @returns Manager
 */
Manager.prototype.stop = function() {
  this.player.stop();
  return this;
};

/**
 * Get or set the experience time
 * @param {Number} value - optional
 * @returns Manager
 */
Manager.prototype.currentTime = function(value) {
  return this.player.currentTime(value);
};

/**
 * Get or set the experience mute state
 * @param {Number} value - optional
 * @returns Manager
 */
Manager.prototype.mute = function(value) {
  return this.player.mute(value);
};

/**
 * Reset the experience
 * @returns Manager
 */
Manager.prototype.reset = function() {
  this.player.reset();
  return this;
};

/**
 * Replay the experience
 * @returns Manager
 */
Manager.prototype.replay = function() {
  this.player.replay();
  return this;
};

/**
 * Get the experience duration
 * @returns Manager
 */
Manager.prototype.duration = function() {
  return this.player.duration();
};

/**
 * Check if the experience is playing
 * @returns Manager
 */
Manager.prototype.isPlaying = function() {
  return this.player.isPlaying();
};

/**
 * Check if the experience is paused
 * @returns Manager
 */
Manager.prototype.isPaused = function() {
  return this.player.isPaused();
};

/**
 * Register an event listener
 * @returns Manager
 */
Manager.prototype.on = function(events, handler) {
  addEventListeners(this._node, events, handler);
  return this;
};

/**
 * Remove an event listener
 * @returns Manager
 */
Manager.prototype.off = function(events, handler) {
  removeEventListeners(this._node, events, handler);
  return this;
};

/* global Invodo, Utility, Elem */
/* exported legacyManager */
'use strict';

// Returns just a Invodo widget not an Ivp Manager instance
function legacyManager(node, config, callback) {
  // A valid dom reference is required
  if (!_.isElement(node)) {
    // throw an error
    throw (Error('You must provide a valid HTMLElement'));
  }
  // RefId are required to load inplayer
  if (!config.hasOwnProperty('refId') || !_.isString(config.refId)) {
    throw (Error('You must provide a valid RefID in your Ivp configuration'));
  }
  // Create a container for InPlayer
  var parent = Elem.container(Utility.uniqueId(), 'ivp-flash-fallback', 'ivp-flash-fallback');
  // Attach the new element
  node.appendChild(parent);
  // Create the Inplayer widget
  return Invodo.Widget.add({
    widgetId: 'IVP_fallback_' + Utility.randString(3),
    parentDomId: parent.id,
    refId: config.refId,
    type: 'inplayer',
    mode: 'embedded',
    onpodload: callback,
    autoplay: 'false',
    chromelessmode: 'false'
  });
}

/* global Elem, Utility, legacyManager, $http */
'use strict';

/**
 * Simple way to create an manager with a default set of recognizers.
 * @param {HTMLElement} element
 * @param {Object} [options]
 * @constructor
 */
function Ivp(node, config, callback) {
  // Check for HTML5 video support
  if (!Utility.supportsVideo()) {
    return legacyManager(node, config, callback);
  }
  // Create a manager and lets get started
  return new Manager(node, config, callback);
}

/**
 * @const {string}
 */
Ivp.version = '{{PKG_VERSION}}';

/**
 * Interactive Video Player's default configuration
 * @namespace
 */
Ivp.defaults = {
  /**
   * Add the theme string as a class name to the widget.
   * This is intended to support simple theming.
   * @NOTE See 'styles/sass/_theme.scss' for the example
   * and default theme, 'ivp-default-theme'.
   * @TODO Need a better name for the default theme :)
  **/
  theme: 'ivp-default-theme',
  /**
   * Add the client namespace to the widget instance. This name
   * is not used in any default styles or themes. It is intended
   * as a simple DOM hook to apply quick styles, and to maintain
   * a cohesive namespace.
   * @NOTE This parameter is the only parameter used to generate
   * a unique ID attribute for the widget instance.
   * @EXAMPLE <div class="invodo-ivp-widget ..." id="invodo-ivp-widget-1">
  **/
  name: 'invodo-ivp-widget',
  /**
   * Add the normalize class, 'ivp-normie' to the widget instance.
   * If you are already properly normalizing your page, and applying
   * box-sizing: border-box to *, you can opt out of the widget
   * normalizing itself. This is intended to reduce unneccessary
   * CSS paints, and class stacking. It is a bit experimental.
  **/
  normalize: true,
  /**
   * Toggle which control elements are enabled
   * If you do not need a control element you can disable
   * them and they will not be created when the widget is
   * instantiated.
   * @NOTE If you pass time: false, the time elements will still
   * be created, but are hidden with a reusable CSS class,
   * in consideration of accessibility.
  **/
  controls: {
    play: true,
    stop: true,
    mute: true,
    replay: true,
    scrub: true,
    time: true
  },
  // Fetch a JSON Ivp config file
  dataPath: null,
  // Change the video load order in MultiClip presentations
  // defaultVideo should contain a valid frame durableId as a string
  defaultVideo: null,
  // This should be removed from user configuration in favor of hardcoding
  // @TODO deprecate this object
  _inplayer: {
    widgetId: 'InvodoWidget_IVP_' + Utility.randString(3),
    autoplay: false,
    // should be doing something with this, triggering a callback
    onpodload: function() {},
    chromelessmode: 'true',
    type: 'inplayer',
    mode: 'embedded'
  }
};

/**
 *
**/
Ivp.defaultTemplate = {
  hotspot: [
    {
      type: 'firefly',
      template: {
        text: '<span class=\"ivp-hotspot-icon\"></span><div class=\"ivp-hotspot-title\" role=\"button\" aria-live=\"polite\"><div><span>${text}</span></div></div>',
        values: ['text']
      },
      position: {x: '', y: ''},
      animate: {
        duration: {
          start: 0.5,
          end: 0.25
        },
        from: {
          y: 10,
          opacity: 0,
          scale: 0.85
        },
        config:[
          {
            y: 0,
            opacity: 1,
            scale: 1
          },
          {
            opacity: 0,
            y: 10
          }
        ]
      }
    }, {
      type: 'notification',
      template: {text: '', values: []},
      position: {x: '', y: ''},
      animate: {}
    }
  ],
  card: [
    {
      type: 'small',
      template: {text: '', values: []},
      position: {x: '', y: ''},
      animate: {}
    }, {
      type: 'medium',
      template: {
        text: '<div class=\"ivp-card--medium\"><div class=\"ivp-card-content\"><div class=\"ivp-container\"><div class=\"ivp-row\"><div class=\"ivp-col-12 ivp-col-pull-gutters ivp-card-image\"><img src=\"${image}\"></div></div><div class=\"ivp-row\"><div class=\"ivp-col-12 ivp-card-text\"><h3>${title}</h3><p class=\"ivp-card-price\">${price}</p><div class=\"ivp-card-ctas\"><a class=\"ivp-card-cta ivp-card-cta--primary\" href=\"${ctaprimarylink}\">${ctaprimarytext}</a><a class=\"ivp-card-cta ivp-card-cta--secondary\" href=\"${ctasecondarylink}\">${ctasecondarytext}</a></div>${text}</div></div></div></div></div>',
        values: ['image', 'title', 'price', 'ctaprimarylink', 'ctaprimarytext', 'ctasecondarylink', 'text']
      },
      position: {
      	x: '0',
      	y: '-54%'
      },
      animate: {
        duration: {
          start: 0.3,
          end: 0.3
        },
        from: {
          opacity: 0,
          top: '50%',
          right: 0
        },
        config:[
          {
            y: '-50%',
            opacity: 1
          },
          {
            opacity: 0,
            y: '-54%'
          }
        ]
      }
    },
    {
      type: 'large',
      template: {
        text: '<div class=\"ivp-card--large\"><div class=\"ivp-card-content\"><div class=\"ivp-container\"><div class=\"ivp-row\"><div class=\"ivp-col-6 ivp-col-pull-gutters ivp-card-image\"><img src=\"${image}\"></div><div class=\"ivp-col-6 ivp-card-text\"><h3>${title}</h3><p class=\"ivp-card-price\">${price}</p><div class=\"ivp-card-ctas\"><a class=\"ivp-card-cta ivp-card-cta--primary\" href=\"${ctaprimarylink}\">${ctaprimarytext}</a><a class=\"ivp-card-cta ivp-card-cta--secondary\" href=\"${ctasecondarylink}\">${ctasecondarytext}</a></div>${text}</div></div></div></div></div>',
        values: ['image', 'title', 'price', 'ctaprimarylink', 'ctaprimarytext', 'ctasecondarylink', 'text']
      },
      position: {
        x: '-50%',
        y: '6%',
      },
      animate: {
        duration: {
          start: 0.3,
          end: 0.3
        },
        from: {
          left: '50%',
          opacity: 0
        },
        config:[
          {
            y: '10%',
            opacity: 1
          },
          {
            opacity: 0,
            y: '7%'
          }
        ]
      }
    }
  ]
};

Ivp.getDefaultCard = function(name) {
  // The default default for Cards is the medium Card
  if (!name) {
    return Ivp.defaultTemplate.card[1];
  }
  var index = _.findIndex(Ivp.defaultTemplate.card, {'type': name});
  return (index > -1) ? Ivp.defaultTemplate.card[index] : Ivp.defaultTemplate.card[1];
};

Ivp.getDefaultHotspot = function(name) {
  // The default default for Cards is the medium Card
  if (!name) {
    return Ivp.defaultTemplate.hotspot[0];
  }
  var index = _.findIndex(Ivp.defaultTemplate.hotspot, {'type': name});
  return (index > -1) ? Ivp.defaultTemplate.hotspot[index] : Ivp.defaultTemplate.hotspot[0];
};

/**
 * the method for registering an Ivp plugin
 *
 * @param  {String} name The name of the plugin
 * @param  {Function} init The function that is run when the player inits
 */
Ivp.plugin = function(name, init) {
  Manager.prototype[name] = init;
};

/**
 * Extend Ivp classes onto the namespace so they can be used individually
 */
Utility.extend(Ivp, {
  // Provides Utility functions like mixin, merge, extend
  Utility: Utility,
  // Provices methods for creating elements
  Element: Elem,
  // Interface for creating an Ivp instance
  Manager: Manager,
  // Wrapper for InPlayer widgets. Normalizes InPlayer quirky events
  Video: Video,
  // Interface for Video
  Player: Player,
  // Interface for adding Controls to Playback modules
  Controls: Controls,
  // Interface for Cards
  Cards: Cards,
  // Interface for adding Hotspots
  Hotspots: HotSpots,
  // Interface for handling Elements
  ElementInterface: ElementInterface,
  // Interface for handling templates,
  Templater: Templater,
  // Interface for XHR Get / Post
  fetch: $http
});

  namespace.Ivp = Ivp;

}(Invodo.Ixd || window));


(function(window, Ivp){
  'use strict';

  var scope, logger, name, podId, pageName;

  /**
   * Log the hotspot.show event
   */
  var onHotSpotShow = function(event) {
    // Submit the userImpression event
    logger.log(name, 'userImpression', {
      imptgt: event.detail.config.title ? event.detail.config.title : '',
      imptype: 'Interactive Hotspot',
      pod: event.detail.pod,
      fr: event.detail.fr,
      playertype: 'shoppable'
    });
  };

  /**
   * Log the hotspot.click event
   */
  var onHotSpotClick = function(event) {
    // Submit the userClick event
    logger.log(name, 'userClick', {
      clktgt: event.detail.config.title ? event.detail.config.title : '',
      clktype: 'Interactive Hotspot',
      pod: event.detail.pod,
      fr: event.detail.fr,
      playertype: 'shoppable'
    });
  };

  /**
   * Log the hotspot.remove event
   */
  var onHotSpotRemove = function(event) {
    // Submit the userClick event
    logger.log(name, 'userClick', {
      clktgt: event.detail.config.title ? event.detail.config.title : '',
      clktype: 'Interactive Hotspot Close',
      pod: event.detail.pod,
      fr: event.detail.fr,
      playertype: 'shoppable'
    });
  };

  /**
   * Log the card.show event
   */
  var onCardShow = function(event) {
    // Submit the userImpression event
    logger.log(name, 'userImpression', {
      imptgt: event.detail.config.title ? event.detail.config.title : '',
      imptype: 'Interactive Card',
      pod: event.detail.pod,
      fr: event.detail.fr,
      playertype: 'shoppable'
    });
  };

  /**
   * Log the card.hide event
   */
  var onCardHide = function(event) {
    // Submit the userImpression event
    logger.log(name, 'userClick', {
      clktgt: event.detail.config.title ? event.detail.config.title : '',
      clktype: 'Interactive Card Close',
      pod: event.detail.pod,
      fr: event.detail.fr,
      playertype: 'shoppable'
    });
  };

  /**
   * Log all card link tags as userClicks
   */
  var onCardCtaClick = function(event) {
    // Find the buttons card
    var card = Ivp.Utility.findParent('ivp-card', event);
    // Find the card config object
    var config = scope.card.getItemById(card.dataset.id);
    // Safely construct the log title key
    var title = config.title ? config.title : 'No Title provided';
    title += ' - ' + event.target.textContent;
    // Submit the userClick event
    logger.log(name, 'userClick', {
      clktgt: title,
      clktype: 'Interactive Card Link',
      pod: podId,
      fr: scope.player.activeFrame().id,
      playertype: 'shoppable'
    });
  };

  /**
   * Log timePlayed events every 5 seconds
   */
  var onTimeUpdate = function(event) {
    // Submit the timePlayed event
    logger.log(name, 'timePlayed', {
      pod: event.detail.pod,
      tm: event.detail.time.toFixed(2),
      fr: event.detail.fr,
      playertype: 'shoppable'
    });
  };

  /**
   * Log a ctaView event
   */
  var onCtaView = function() {
    // Submit the ctaView event
    logger.log(name, 'ctaView', {
      pn: pageName,
      pod: podId,
      playertype: 'shoppable'
    });
  };

  /**
   * Create the logging plugin.
   * @param options (optional) {object} configuration for the plugin
   */
  var init = function(elements) {
    // we need Ivp methods for some log handlers
    scope = this;
    // store the invodo logger utility
    logger = Invodo.Module.get('logger');
    // get the instance settings for the widgetName, podId, and pageName
    var settings = scope.settings();
    // store the podId
    podId = settings.podConfig.pod;
    // store the widgetName
    name = settings.name;
    // store the pageName
    pageName = settings.podConfig.pageName;
    // Log a ctaView immediately
    onCtaView();
    // Register listener for hotspot.show event
    scope.on('hotspot.show', onHotSpotShow);
    // Register listener for hotspot.click event
    scope.on('hotspot.click', onHotSpotClick);
    // Register listener for hotspot.remove event
    scope.on('hotspot.remove', onHotSpotRemove);
    // Register listener for card.show event
    scope.on('card.show', onCardShow);
    // Register listener for card.hide event
    scope.on('card.hide', onCardHide);
    // Register listener for timePlayed event - throttled to 5 seconds
    scope.on('timeupdate', Ivp.Utility.throttle(onTimeUpdate, 5000));
    // Get all link tags inside cards and attach click events
    _.forEach(elements.cards.querySelectorAll('.ivp-card a'), function(btn) {
      btn.addEventListener('click', onCardCtaClick);
    });
  };

  // register the plugin
  Ivp.plugin('logging', init);

})(window, window.Invodo.Ixd.Ivp);
